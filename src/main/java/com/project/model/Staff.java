package com.project.model;

public class Staff {
	
	private String userId;
	private String nama;
	private String password;
	private String email;
	private String flag;
	private String menuAccess;
	private String errorMessage;
	private String username;
	private String namaMerchant;
	private String isDeleted;
	private String merchantStatus;
	private String merchantId;
	private String passwordBaru;
	private String merchantAccountNumber;
	private String response;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getMenuAccess() {
		return menuAccess;
	}
	public void setMenuAccess(String menuAccess) {
		this.menuAccess = menuAccess;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getNamaMerchant() {
		return namaMerchant;
	}
	public void setNamaMerchant(String namaMerchant) {
		this.namaMerchant = namaMerchant;
	}
	
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public String getMerchantStatus() {
		return merchantStatus;
	}
	public void setMerchantStatus(String merchantStatus) {
		this.merchantStatus = merchantStatus;
	}
	
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	
	public String getPasswordBaru() {
		return passwordBaru;
	}
	public void setPasswordBaru(String passwordBaru) {
		this.passwordBaru = passwordBaru;
	}
	
	public String getMerchantAccountNumber() {
		return merchantAccountNumber;
	}
	public void setMerchantAccountNumber(String merchantAccountNumber) {
		this.merchantAccountNumber = merchantAccountNumber;
	}
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
	@Override
	public String toString() {
		return "Staff [userId=" + userId + ", nama=" + nama + ", password=" + password + ", email=" + email + ", flag="
				+ flag + ", menuAccess=" + menuAccess + ", errorMessage=" + errorMessage + ", username=" + username
				+ ", namaMerchant=" + namaMerchant + ", isDeleted=" + isDeleted + ", merchantStatus=" + merchantStatus
				+ ", merchantId=" + merchantId + ", passwordBaru=" + passwordBaru + ", merchantAccountNumber="
				+ merchantAccountNumber + ", response=" + response + "]";
	}
	
	
}
