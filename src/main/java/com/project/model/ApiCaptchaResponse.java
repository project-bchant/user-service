package com.project.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiCaptchaResponse {
	private String success;
	
	private Date challenge_ts;  // timestamp of the challenge load (ISO format yyyy-MM-dd'T'HH:mm:ssZZ)
	
	private String hostname;
	
	@JsonProperty("error-codes")
	private String[] errorCodes;

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public Date getChallenge_ts() {
		return challenge_ts;
	}

	public void setChallenge_ts(Date challenge_ts) {
		this.challenge_ts = challenge_ts;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String[] getErrorCodes() {
		return errorCodes;
	}

	public void setErrorCodes(String[] errorCodes) {
		this.errorCodes = errorCodes;
	}

	@Override
	public String toString() {
		return "ApiCaptchaResponse [success=" + success + ", challenge_ts=" + challenge_ts + ", hostname=" + hostname
				+ ", errorCodes=" + errorCodes + "]";
	}
	
}
