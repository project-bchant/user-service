package com.project.dao;

import java.security.SecureRandom;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
/*import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;*/
import org.springframework.stereotype.Repository;

import com.project.dbconn.getDbConnection;
import com.project.model.Merchant;
import com.project.model.Staff;

import oracle.jdbc.OracleTypes;

@Repository
public class StaffDaoImpl /*extends JdbcDaoSupport*/ implements StaffDao{

	private static final Logger LOGGER = LoggerFactory.getLogger(StaffDaoImpl.class);
/*	@Autowired 
	DataSource dataSource;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@PostConstruct
	private void initialize(){
		setDataSource(dataSource);
	}*/
	
/*	@Autowired 
	BCrypt bcrypt;*/
	
	@Autowired
    private JavaMailSender sender;
	
	Connection conn = null;
	PreparedStatement pst = null;
	CallableStatement callableStatement = null;
	ResultSet rs = null;
	ResultSet rs2 = null;
	PreparedStatement pst2 = null;
	
	static final SecureRandom rnd = new SecureRandom();
	
	@Value("${AB}")
	String AB;
	
	@Value("${passwordLength}")
	int passwordLength;
	
	@Value("${staff.queryGetAllStaff}")
	String queryGetAllStaff;
		
	@Value("${staff.queryGetDetailStaff}")
	String queryGetDetailStaff;
	
	@Value("${staff.queryGetAllActiveStaffCount}")
	String queryGetAllActiveStaffCount;
	
	@Value("${staff.queryCheckValidPassword}")
	String queryCheckValidPassword;
	
	@Value("${staff.queryGetMerchantDetail}")
	String queryGetMerchantDetail;
	
	@Value("${staff.queryTest}")
	String queryTest;
		
	@Override
	public List<Staff> getAllStaffs(Staff staff) {
		
		/*String query = "SELECT u.userid, u.nama, u.email, u.menuaccess, u.flag, u.username, m.merchantname "
				+ "FROM USERR u, merchant m WHERE m.merchantid = u.flag and u.flag = ? and u.isdeleted = '0' and u.isOwner <> 1"
				+ "ORDER BY u.USERID ASC";*/
		
		List<Staff> liststaff = new ArrayList<Staff>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetAllStaff + " ...");
			
			pst = conn.prepareStatement(queryGetAllStaff);
			LOGGER.info("Prepared Statement dengan isi " + queryGetAllStaff + " berhasil dijalanakan ...");
			
			pst.setString(1, staff.getFlag());
			
			rs = pst.executeQuery();
			LOGGER.info("Mengeksekusi pst dengan hasil tangkapan -> " + staff.toString() + " ...");
	
			if(!rs.isBeforeFirst()) {
				LOGGER.info("Merchant dengan merchantId " + staff.getFlag() + " tidak mempunyai data staff ...");
				staff = new Staff();
				staff.setErrorMessage("Tidak ada data staff");
			}
			else {
				while(rs.next()){
					staff = new Staff();
					staff.setUserId(rs.getString("userid"));
					staff.setNama(rs.getString("nama"));
					staff.setUsername(rs.getString("username"));
					staff.setEmail(rs.getString("email"));
					staff.setFlag(rs.getString("flag"));
					staff.setMenuAccess(rs.getString("menuaccess"));
					staff.setNamaMerchant(rs.getString("merchantname"));
					liststaff.add(staff);
				}
				LOGGER.info("Berhasil memasukkan data staff ke dalam listStaff");
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return liststaff;
		
	}

	@Override
	public Integer insertStaff(Staff staff) {
		// TODO Auto-generated method stub
		String spName = "INSERT_USER";
		Integer resultinsert = 0;
		try{
			
			//generatePassword
			String password = generatePassword(passwordLength); //Ricky123
			LOGGER.info("Password yang digenerate -> " + password + " ...");
			staff.setPassword(password);
			LOGGER.info("set staffPassword -> " + staff.getPassword() + " ...");
			
			conn = getDbConnection.Access(); //Get Connection
			
			//StoredProcedure
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?,?,?,?,?)}");
			callableStatement.setString(1, staff.getNama());
			callableStatement.setString(2, BCrypt.hashpw(password, BCrypt.gensalt())); //$2a$10$asdhjvuej
			LOGGER.info("Salt -> " + BCrypt.gensalt());
			callableStatement.setString(3, staff.getEmail());
			callableStatement.setString(4, staff.getFlag()); //auto-generate dari jefry
			callableStatement.setString(5, staff.getMenuAccess());
			LOGGER.info("Menu aksess -> " + staff.getMenuAccess() + " ...");
			callableStatement.setString(6, staff.getUsername());
			
			callableStatement.registerOutParameter(7, OracleTypes.CURSOR);
			
			resultinsert = callableStatement.executeUpdate();
			
			rs = (ResultSet) callableStatement.getObject(7);
			
			if(!rs.isBeforeFirst()) {
				//data kosong
				LOGGER.error("Cek di Stored Procedure Insert, terjadi kegagalan saat select di terakhir ...");
			}
			else {
				while(rs.next()) {
					LOGGER.info("Error message dari db -> " + rs.getString("v_error_msg") + " ...");
					if(rs.getInt("v_error_msg_numb") == 1) {
						
						if(resultinsert == 1) { //insert berhasil
							LOGGER.info("Berhasil melakukan insert staff dengan email " + staff.getEmail() + " ...");
							/*			boolean sendmail =  sendMail(staff);
							if(sendmail == false) {
								resultinsert = 0;
								LOGGER.error("Gagal mengirimkan email ...");
								return resultinsert;
							} */
						}
						else {
							LOGGER.error("Gagal melakukan insert, coba cek StaffDaoImpl insertStaff ...");
							/*boolean sendfailedmail = sendFailedMail(staff);
							LOGGER.info("Menjalankan function sendFailedMail ...");
							if(sendfailedmail == false) {
								resultinsert = 0;
								LOGGER.error("Gagal mengirimkan email ...");
								return resultinsert;
							}*/
						}
					}
					else if(rs.getInt("v_error_msg_numb") == 0){ //email sudah terdaftar
						staff.setErrorMessage((rs.getString("v_error_msg")));
						LOGGER.error("Error di db -> " + staff.getErrorMessage() + " ...");
						resultinsert = 0;
					}
					else if(rs.getInt("v_error_msg_numb") == -1){ //username sudah terdaftar
						staff.setErrorMessage((rs.getString("v_error_msg")));
						LOGGER.error("Error di db -> " + staff.getErrorMessage() + " ...");
						resultinsert = -1;
					}
					else if(rs.getInt("v_error_msg_numb") == -2){ //email dan username sudah terdaftar
						staff.setErrorMessage((rs.getString("v_error_msg")));
						LOGGER.error("Error di db -> " + staff.getErrorMessage() + " ...");
						resultinsert = -2;
					}
				}
			}
			return resultinsert;
		} catch(Exception e) {
			System.out.println("error -> " +  e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return resultinsert;
	}

	@Override
	public Staff deleteAllStaff(Staff staff) {
		// TODO Auto-generated method stub
		String spName = "DELETE_ALL_USER";
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
	
			conn = getDbConnection.Access(); //Get Connection
			
			//StoredProcedure
			callableStatement = conn.prepareCall("{call " + spName + "(?)}");
			
			callableStatement.setString(1, staff.getFlag());
			
			int a = callableStatement.executeUpdate();
			if(a == 1) {
				staff.setNama("Berhasil delete staff");
				staff.setUserId("Berhasil delete staff");
				staff.setPassword("Berhasil delete staff");
				staff.setEmail("Berhasil delete staff");
				staff.setFlag("Berhasil delete staff");
				staff.setMenuAccess("Berhasil delete staff");
			}
			return staff;
		} catch(Exception e) {
			System.out.println("error ->" +  e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return staff;
	}

	@Override
	public Integer updateStaff(Staff staff) {
		// TODO Auto-generated method stub
		String spName = "UPDATE_USER";
		Integer resultupdate = 0;
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
	
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			//StoredProcedure
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?,?,?,?)}");
			LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");
			
			callableStatement.setString(1, staff.getUserId());
			callableStatement.setString(2, staff.getNama());
			callableStatement.setString(3, staff.getFlag());
			callableStatement.setString(4, staff.getMenuAccess());
			callableStatement.setString(5, staff.getEmail());
			callableStatement.registerOutParameter(6, OracleTypes.CURSOR); 
			LOGGER.info("Set staff ke dalam callableStatement -> " + staff.toString() + " ...");
			
			callableStatement.executeUpdate();
			LOGGER.info("Mengeksekusi stored procedure " + spName + " ...");
			
			rs = (ResultSet) callableStatement.getObject(6);
			LOGGER.info("Berhasil mendapatkan hasil -> " + rs.toString() + " ...");
			
			if(!rs.isBeforeFirst()) {
				staff.setErrorMessage("Terjadi kesalahan pada stored procedure " + spName + " ...");
				LOGGER.error("Error message -> " + staff.getErrorMessage() + " ...");
			}
			else {
				while(rs.next()) {
					if(rs.getInt("v_error_msg_numb") == 0) {
						resultupdate = rs.getInt("v_error_msg_numb");
						staff.setErrorMessage(rs.getString("v_error_msg"));
						LOGGER.error("Error message dari db -> " + staff.getErrorMessage() + " ...");
					}
					else if(rs.getInt("v_error_msg_numb") == 1) {
						resultupdate = rs.getInt("v_error_msg_numb");
						staff.setErrorMessage(rs.getString("v_error_msg"));
						LOGGER.info("Info message dari db -> " + staff.getErrorMessage() + " ...");
					}
				}
			}
		} catch(Exception e) {
			System.out.println("error -> " +  e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return resultupdate;
	}

	@Override
	public Integer deleteStaffById(Staff staff) {
		// TODO Auto-generated method stub
		String spName = "DELETE_USER_BY_ID";
		Integer resultdelete = 0;
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
	
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			//StoredProcedure
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?)}");
			LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");
			
			callableStatement.setString(1, staff.getUserId());
			callableStatement.setString(2, staff.getFlag());
			callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
			LOGGER.info("Set staff ke dalam callableStatement -> " + staff.toString() + " ...");
			
			callableStatement.executeUpdate();
			LOGGER.info("Mengeksekusi stored procedure " + spName + " ...");
			
			rs = (ResultSet) callableStatement.getObject(3);
			LOGGER.info("Berhasil mendapatkan hasil -> " + rs.toString() + " ...");
			
			if(!rs.isBeforeFirst()) {
				LOGGER.error("Gagal mendapatkan hasil delete row ...");
			}
			else {
				while(rs.next()) {
					if(rs.getInt("deleted_row_count") == 0) {
						resultdelete = 0;
						LOGGER.error("Delete gagal, userId / merchantId tidak ditemukan ...");
						return resultdelete;
					}
					else if(rs.getInt("deleted_row_count") == 1) {
						resultdelete = 1;
						LOGGER.info("Delete berhasil ...");
						return resultdelete;
					}
					else if (rs.getInt("deleted_row_count") > 1) {
						resultdelete = rs.getInt("deleted_row_count");
						LOGGER.error("Ada " + String.valueOf(resultdelete) + " baris yang terupdate ...");
						return resultdelete;
					}
				}
			}
			
		} catch(Exception e) {
			System.out.println("error -> " +  e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return resultdelete;
	}

	@Override
	public Integer deleteStaffByIdIsDeleted(Staff staff) {
		// TODO Auto-generated method stub
		String spName = "UPDATE_USER_FOR_DELETE";
		Integer resultdelete = 0;
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
	
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			//StoredProcedure
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?)}");
			LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");
			
			callableStatement.setString(1, staff.getUserId());
			callableStatement.setString(2, staff.getFlag());
			callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
			LOGGER.info("Set staff ke dalam callableStatement -> " + staff.toString() + " ...");
			
			callableStatement.executeUpdate();
			LOGGER.info("Mengeksekusi stored procedure " + spName + " ...");
			
			rs = (ResultSet) callableStatement.getObject(3);
			LOGGER.info("Berhasil mendapatkan hasil -> " + rs.toString() + " ...");
			
			if(!rs.isBeforeFirst()) {
				LOGGER.error("Gagal mendapatkan hasil delete row ...");
			}
			else {
				while(rs.next()) {
					if(rs.getInt("updated_row_count") == 0) {
						resultdelete = 0;
						LOGGER.error("Delete gagal, userId / merchantId tidak ditemukan ...");
						return resultdelete;
					}
					else if(rs.getInt("updated_row_count") == 1) {
						resultdelete = 1;
						LOGGER.info("Delete berhasil ...");
						return resultdelete;
					}
					else if (rs.getInt("updated_row_count") > 1) {
						resultdelete = rs.getInt("updated_row_count");
						LOGGER.error("Ada " + String.valueOf(resultdelete) + " baris yang terupdate ...");
						return resultdelete;
					}
				}
			}
			
		} catch(Exception e) {
			System.out.println("error -> " +  e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return resultdelete;
	}
	
	@Override
	public List<Staff> testRicky(Staff staff) {
		String spName = "TEST_RICKY";
		
		List<Staff> liststaff = new ArrayList<Staff>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection

			//StoredProcedure
			callableStatement = conn.prepareCall("{call " + spName + "(?)}");
			callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
			callableStatement.executeUpdate();
	
			rs = (ResultSet) callableStatement.getObject(1);
			
			while(rs.next()){
				staff = new Staff();
				staff.setUserId(rs.getString("userid"));
				staff.setNama(rs.getString("nama"));
				staff.setPassword(rs.getString("userpassword"));
				staff.setEmail(rs.getString("email"));
				staff.setFlag(rs.getString("flag"));
				staff.setMenuAccess(rs.getString("menuaccess"));
				liststaff.add(staff);
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return liststaff;
	}
	
	@Override
	public Staff getDetailStaff(Staff staff) {
		
		/*String query = "SELECT u.userid, u.nama, u.email, u.menuaccess, u.flag, u.username, m.merchantname"
				+ " FROM USERR u, merchant m"
				+ " WHERE m.merchantid = u.flag and u.USERID = ? and u.flag = ?";*/
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetDetailStaff + " ...");
			
			pst = conn.prepareStatement(queryGetDetailStaff);
			LOGGER.info("Prepared Statement dengan isi " + queryGetDetailStaff + " berhasil dijalanakan ...");
			
			pst.setString(1, staff.getUserId());
			pst.setString(2, staff.getFlag());
			
			rs = pst.executeQuery();
			LOGGER.info("Mengeksekusi pst dengan hasil tangkapan -> " + staff.toString() + " ...");
			
			if(!rs.isBeforeFirst()) {
				LOGGER.info("Merchant dengan merchantId " + staff.getFlag() + " tidak mempunyai staff dengan Id " + staff.getUserId() + " ...");
				staff = new Staff();
			}
			else {
				while(rs.next()) {
					staff = new Staff();
					staff.setUserId(rs.getString("userid"));
					staff.setNama(rs.getString("nama"));
					staff.setEmail(rs.getString("email"));
					staff.setFlag(rs.getString("flag"));
					staff.setMenuAccess(rs.getString("menuaccess"));
					staff.setUsername(rs.getString("username"));
					staff.setNamaMerchant(rs.getString("merchantname"));
					LOGGER.info(staff.toString());
				}
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return staff;
		
	}
	
	public String generatePassword(int len) {
		Staff staff = new Staff();
		StringBuilder sb = new StringBuilder(len);
		for( int i = 0; i < len; i++ ) {
		sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		}
		staff.setPassword(sb.toString());
		return staff.getPassword();
	}
	
	public boolean sendMail(Staff staff) {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
            helper.setTo(staff.getEmail());
            LOGGER.info("SEND EMAIL KE TUJUAN -> " + staff.getEmail());
            LOGGER.info("Username -> " + staff.getUsername());
            helper.setText("Terimakasih sudah mendaftar, Harap login menggunakan email/username dibawah ini \n\n"
            		+ "Username: " + staff.getUsername() + "\n"
            		+ "Email: " + staff.getEmail() + " \n"
            		+ "Password: " + staff.getPassword());
            LOGGER.info("Berhasil set helper text ...");
            helper.setSubject("Pendaftaran BCA Merchant");
            LOGGER.info("Berhasil set helper subject ...");
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
        finally {
	        sender.send(message);
	        LOGGER.info("BERHASIL SEND EMAIL ...");
        }
        return true;
    }

	public boolean sendNewPassword(Staff staff) {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
            helper.setTo(staff.getEmail());
            LOGGER.info("SEND EMAIL KE TUJUAN -> " + staff.getEmail());
            helper.setText("Berikut password baru untuk akun anda, Harap login menggunakan email dibawah ini \n\n"
            		+ "Email: " + staff.getEmail() + " \n"
            		+ "Password: " + staff.getPassword());
            LOGGER.info("Berhasil set helper text -> " + "Berikut password baru untuk akun anda, Harap login menggunakan email dibawah ini \n\n"
            		+ "Email: " + staff.getEmail() + " \n"
            		+ "Password: " + staff.getPassword());
            helper.setSubject("Pendaftaran BCA Merchant");
            LOGGER.info("Berhasil set helper subject ...");
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
        finally {
	        sender.send(message);
	        LOGGER.info("BERHASIL SEND EMAIL ...");
        }
        return true;
    }
	
	public boolean sendFailedMail(Staff staff) {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
            helper.setTo(staff.getEmail());
            LOGGER.info("SEND EMAIL KE TUJUAN -> " + staff.getEmail());
            LOGGER.info("Username -> " + staff.getUsername());
            helper.setText("Mohon maaf, pendaftaran akun dengan Username " + staff.getUsername() + " "
            		+ "dan Email " + staff.getEmail() + " sedang mengalami gangguan ...");
            LOGGER.info("Text yang dikirim -> " + "Mohon maaf, pendaftaran akun dengan Username " + staff.getUsername() + " "
            		+ "dan Email " + staff.getEmail() + " sedang mengalami gangguan ...");
            helper.setSubject("Pendaftaran BCA Merchant");
            LOGGER.info("Berhasil set helper subject ...");
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
        finally {
	        sender.send(message);
	        LOGGER.info("BERHASIL SEND EMAIL ...");
        }
        return true;
    }
	
	public boolean sendRejectedMail(Staff staff) {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
            helper.setTo(staff.getEmail());
            LOGGER.info("SEND EMAIL KE TUJUAN -> " + staff.getEmail());
            LOGGER.info("Username -> " + staff.getUsername());
            helper.setText("Mohon maaf, pendaftaran merchant dengan akun Username " + staff.getUsername() + " "
            		+ "dan Email " + staff.getEmail() + " anda belum bisa dilanjutkan ke proses berikutnya ...");
            helper.setSubject("Pendaftaran BCA Merchant");
            LOGGER.info("Berhasil set helper subject ...");
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
        finally {
	        sender.send(message);
	        LOGGER.info("BERHASIL SEND EMAIL ...");
        }
        return true;
    }
	
	@Override
	public Integer getAllActiveStaffsCount(Staff staff) {
		
		/*String query = "SELECT count(u.username) as jumlah"
				+ " FROM USERR u, merchant m "
				+ "WHERE m.merchantid = u.flag and u.flag = ? and u.ISDELETED = '0'"
				+ " ORDER BY u.USERID ASC";*/
		Integer jumlah = null;
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetAllActiveStaffCount + " ...");
			
			pst = conn.prepareStatement(queryGetAllActiveStaffCount);
			LOGGER.info("Prepared Statement dengan isi " + queryGetAllActiveStaffCount + " berhasil dijalanakan ...");
			
			pst.setString(1, staff.getFlag());
			
			rs = pst.executeQuery();
			LOGGER.info("Mengeksekusi pst dengan hasil tangkapan -> " + staff.toString() + " ...");
			
			if(!rs.isBeforeFirst()) {
				LOGGER.info("Merchant dengan merchantId " + staff.getFlag() + " tidak mempunyai staff aktif...");
				staff = new Staff();
				staff.setErrorMessage("Tidak ada data staff");
			}
			else {
				while(rs.next()) {
					jumlah = rs.getInt("jumlah");
					LOGGER.info("Jumlah user yang aktif untuk merchant dengan merchantId " + staff.getFlag() + " adalah " + jumlah + " user ...");
				}
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return jumlah;
		
	}
	
	@Override
	public Staff changePassword(String userId, String flag, String password, String passwordBaru) {
		String spName = "CHANGE_PASSWORD";
		Staff staff = new Staff();
		/*String query = "SELECT userpassword from userr where userid = ? and flag = ?";*/
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryCheckValidPassword + " ...");
			
			pst = conn.prepareStatement(queryCheckValidPassword);
			LOGGER.info("Prepared Statement dengan isi " + queryCheckValidPassword + " berhasil dijalanakan ...");
			
			pst.setString(1, userId);
			pst.setString(2, flag);
			
			String passwordlama = password; //passwordlama dari front end
			rs = pst.executeQuery();
			
			if(!rs.isBeforeFirst()) {
				staff.setErrorMessage("User tidak ditemukan");
				LOGGER.error("Error message -> " + staff.getErrorMessage() + " ...");
			}
			else {
				while(rs.next()) {
					String passwordDiDb = rs.getString("userpassword"); //password lama dari db (hashed)
					LOGGER.info("password di db -> " + passwordDiDb);
					if(BCrypt.checkpw(passwordlama , passwordDiDb)) {	//kalo cocok
						LOGGER.info("Password cocok ...");
						LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
						
						String passwordbaru = passwordBaru;
						LOGGER.info("Password barunya -> " + passwordbaru + " ...");
						//StoredProcedure
						callableStatement = conn.prepareCall("{call " + spName + "(?,?,?,?)}");
						callableStatement.setString(1, userId);
						callableStatement.setString(2, flag);
						callableStatement.setString(3, BCrypt.hashpw(passwordbaru, BCrypt.gensalt()));
						LOGGER.info("Password setelah di hash -> " + BCrypt.hashpw(passwordbaru, BCrypt.gensalt()) + " ...");
						callableStatement.registerOutParameter(4, OracleTypes.CURSOR);
						
						callableStatement.executeUpdate();
						
						rs = (ResultSet) callableStatement.getObject(4);
						
						if(!rs.isBeforeFirst()) {
							LOGGER.info("Terjadi kegagalan pada query ... ");
							staff.setErrorMessage("Terjadi kegagalan pada query ...");
						}
						else {
							while(rs.next()) {
								if(rs.getInt("v_error_msg_numb") == 0) {
									staff.setErrorMessage(rs.getString("v_error_msg"));
									LOGGER.error("Error message -> " + staff.getErrorMessage() + " ...");
								}
								else if(rs.getInt("v_error_msg_numb") == 1) {
									staff.setErrorMessage(rs.getString("v_error_msg"));
									LOGGER.error("Error message -> " + staff.getErrorMessage() + " ...");
								}
								else {
									LOGGER.error("Terjadi kesalahan di stored procedure, silahkan cek SP -> " + spName + " ...");
									staff.setErrorMessage("Terjadi kesalahan di stored procedure, silahkan cek SP -> " + spName + " ...");
								}
							}
						}
					}
					else {
						staff.setErrorMessage("Password tidak sesuai");
						LOGGER.info("password lama -> " + password);
						LOGGER.info("password baru -> " + passwordBaru);
						LOGGER.error("Error message -> " + staff.getErrorMessage() + " ...");
					}
				}
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
				
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return staff;
	}
	
	@Override
	public Staff forgetPassword(Staff staff) {
		String spName = "FORGET_PASSWORD";
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			
			//generatePassword
			String password = generatePassword(passwordLength); //Ricky123
			LOGGER.info("Password yang digenerate -> " + password + " ...");
			staff.setPassword(password);
			LOGGER.info("set staffPassword -> " + staff.getPassword() + " ...");
			
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			//StoredProcedure
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?,?)}");
			callableStatement.setString(1, staff.getUsername());
			callableStatement.setString(2, staff.getEmail());
			callableStatement.setString(3, BCrypt.hashpw(password, BCrypt.gensalt()));
			LOGGER.info("Password setelah di hash -> " + BCrypt.hashpw(password, BCrypt.gensalt()) + " ...");
			callableStatement.registerOutParameter(4, OracleTypes.CURSOR);
			
			LOGGER.info("email -> " + staff.getEmail() + " ...");
			if(staff.getEmail() == null) {
				LOGGER.error("Email kosong, harus diisi ...");
				staff.setErrorMessage("Email kosong, harus diisi");
				return staff;
			}
			else {
				/*boolean sendnewpassword =  sendNewPassword(staff);
				if(sendnewpassword == false) {
					resultforgetpassword = 0;
					LOGGER.error("Gagal mengirimkan email ...");
					return resultforgetpassword;
				} */
			}
			callableStatement.executeUpdate();
			
			rs = (ResultSet) callableStatement.getObject(4);
			
			if(!rs.isBeforeFirst()) {
				LOGGER.info("Terjadi kegagalan pada query ... ");
				staff.setErrorMessage("Terjadi kegagalan pada query");
				return staff;
			}
			else {
				while(rs.next()) {
					if(rs.getInt("v_error_msg_numb") == 0) {
						LOGGER.error("Error msg -> " + rs.getString("v_error_msg") + " ...");
						staff.setErrorMessage(rs.getString("v_error_msg"));
					}
					else if(rs.getInt("v_error_msg_numb") == 1) {
						LOGGER.error("Error msg -> " + rs.getString("v_error_msg") + " ...");
						staff.setErrorMessage(rs.getString("v_error_msg"));
					}
					else if (rs.getInt("v_error_msg_numb") > 1) {
						LOGGER.error("Terjadi kesalahan pada stored procedure " + spName + " ...");
						staff.setErrorMessage("Terjadi kesalahan pada stored procedure " + spName);
					}
				}
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return staff;
	}

	@Override
	public Staff approveMerchant(Staff staff) {
		// update table merchant, status jadi aktif
		// insert usernya
		// send email
		String spName = "APPROVE_MERCHANT";
		String merchantid = staff.getFlag();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
	
			conn = getDbConnection.Access(); //Get Connection
			conn.setAutoCommit(false);
			LOGGER.info("Membuka koneksi database ...");
			
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			//generatePassword
			String password = generatePassword(passwordLength); //Ricky123
			LOGGER.info("Password yang digenerate -> " + password + " ...");
			staff.setPassword(password);
			LOGGER.info("set staffPassword -> " + staff.getPassword() + " ...");
			
			//StoredProcedure
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?,?,?,?,?,?)}");
			LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");
			
			callableStatement.setString(1, staff.getMerchantStatus());
			callableStatement.setString(2, staff.getMerchantId());
			callableStatement.setString(3, staff.getNama());
			callableStatement.setString(4, BCrypt.hashpw(password, BCrypt.gensalt()));
			callableStatement.setString(5, staff.getEmail());
			callableStatement.setString(6, staff.getUsername());
			callableStatement.setString(7, staff.getMerchantAccountNumber());
			
			callableStatement.registerOutParameter(8, OracleTypes.CURSOR); 
			
			callableStatement.executeUpdate();
			LOGGER.info("Mengeksekusi stored procedure " + spName + " ...");
			
			rs = (ResultSet) callableStatement.getObject(8);
			
			if(!rs.isBeforeFirst()) {
				LOGGER.error("Terjadi kegagalan pada stored procedure " + spName + " ...");
				staff.setErrorMessage("Terjadi kegagalan pada stored procedure " + spName + " ...");
			}
			else {
				while(rs.next()) {
				
					if(rs.getInt("v_err_msg_numb") == 0) { //gagal update
						staff.setErrorMessage(rs.getString("v_err_msg"));
						LOGGER.error("Error message -> " + staff.getErrorMessage() + " ...");
					}
					else if(rs.getInt("v_err_msg_numb") == 1) { //updated, approved
						boolean sendmail =  sendMail(staff);
						if(sendmail == false) {
							LOGGER.error("Gagal mengirimkan email ...");
							staff.setErrorMessage("Gagal mengirimkan email");
						}
						conn.commit();
						LOGGER.info("menjalankan commit");
						staff.setErrorMessage(rs.getString("v_err_msg"));
						LOGGER.info("Info message -> " + staff.getErrorMessage() + " ...");
					}
					else if(rs.getInt("v_err_msg_numb") == -1) { //updated tapi reject
						boolean sendrejectedmail =  sendRejectedMail(staff);
						if(sendrejectedmail == false) {
							LOGGER.error("Gagal mengirimkan email ...");
							staff.setErrorMessage("Gagal mengirimkan email");
						} 
						conn.commit();
						LOGGER.info("menjalankan commit");
						staff.setErrorMessage(rs.getString("v_err_msg"));
						LOGGER.info("Info message -> " + staff.getErrorMessage() + " ...");
					}
				}
			}
		} catch(Exception e) {
			System.out.println("error -> " +  e.getMessage());
			e.printStackTrace();
			try {
				LOGGER.info("menjalankan rollback");
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}

			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return staff;
	}

	@Override
	public Staff updateMerchant(String merchantId, String alamat, String detaillocation, String location, String lat, String lng,
			String mobilenumber) {
		Staff staff = new Staff();
		String spName = "UPDATE_MERCHANT";
		try{
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			//StoredProcedure
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?,?,?,?,?,?)}");
			callableStatement.setString(1, merchantId);
			callableStatement.setString(2, alamat);
			callableStatement.setString(3, detaillocation);
			callableStatement.setString(4, location);
			callableStatement.setString(5, lat);
			callableStatement.setString(6, lng);
			callableStatement.setString(7, mobilenumber);
			callableStatement.registerOutParameter(8, OracleTypes.CURSOR);
			
			callableStatement.executeUpdate();
			
			rs = (ResultSet) callableStatement.getObject(8);
			
			if(!rs.isBeforeFirst()) {
				LOGGER.info("Terjadi kegagalan pada query ... ");
				staff.setErrorMessage("Terjadi kegagalan pada query");
				return staff;
			}
			else {
				while(rs.next()) {
					if(rs.getInt("v_error_msg_numb") == 0) {
						LOGGER.error("Error msg -> " + rs.getString("v_error_msg") + " ...");
						staff.setErrorMessage(rs.getString("v_error_msg"));
					}
					else if(rs.getInt("v_error_msg_numb") == 1) {
						LOGGER.error("Error msg -> " + rs.getString("v_error_msg") + " ...");
						staff.setErrorMessage(rs.getString("v_error_msg"));
					}
					else{
						LOGGER.error("Error msg -> " + rs.getString("v_error_msg") + " ...");
						staff.setErrorMessage(rs.getString("v_error_msg"));
					}
				}
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return staff;
	}

	@Override
	public Merchant getMerchantForUpdate(String merchantId) {
		Merchant merchant = new Merchant();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetMerchantDetail + " ...");
			
			pst = conn.prepareStatement(queryGetMerchantDetail);
			LOGGER.info("Prepared Statement dengan isi " + queryGetMerchantDetail + " berhasil dijalanakan ...");
			
			pst.setString(1, merchantId);
			
			rs = pst.executeQuery();
			
			if(!rs.isBeforeFirst()) {
				LOGGER.info("Merchant dengan merchantId " + merchantId + " tidak ditemukan ...");
				merchant.setErrorMessage("Merchant tidak ditemukan");
			}
			else {
				while(rs.next()) {
					merchant = new Merchant();
					merchant.setMerchantId(merchantId);
					merchant.setAddress(rs.getString("address"));
					merchant.setDetaillocation(rs.getString("detaillocation"));
					merchant.setEmail(rs.getString("email"));
					merchant.setLocation(rs.getString("location"));
					merchant.setMerchantName(rs.getString("merchantname"));
					merchant.setMobileNumber(rs.getString("mobilenumber"));
					merchant.setOwnerName(rs.getString("ownername"));
					merchant.setRekMerchant(rs.getString("rekmerchant"));
					merchant.setStatus(rs.getString("status"));
					merchant.setUsername(rs.getString("username"));
					merchant.setDescription(rs.getString("description"));
					merchant.setCreatedat(rs.getString("createdat"));
					merchant.setLat(rs.getString("lat"));
					merchant.setLng(rs.getString("lng"));
					merchant.setArea(rs.getString("area"));
					merchant.setPlaceid(rs.getString("placeid"));
					merchant.setErrorMessage("");
				}
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return merchant;
	}

	@Override
	public List<Staff> testSelect() {
		List<Staff> listStaff = new ArrayList<Staff>();
		Staff staff = new Staff();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			
			pst = conn.prepareStatement(queryTest);
			LOGGER.info("Prepared Statement dengan isi " + queryTest + " berhasil dijalanakan ...");
			
			rs = pst.executeQuery();
			
			if(!rs.isBeforeFirst()) {
				staff.setErrorMessage("Merchant tidak ditemukan");
			}
			else {
				while(rs.next()) {
					staff = new Staff();
					staff.setUserId(rs.getString("userid"));
					staff.setNama(rs.getString("nama"));
					staff.setPassword(rs.getString("userpassword"));
					staff.setEmail(rs.getString("email"));
					staff.setFlag(rs.getString("flag"));
					staff.setMenuAccess(rs.getString("menuaccess"));
					listStaff.add(staff);
				}
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pst != null) {
					pst.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return listStaff;
	}

}
