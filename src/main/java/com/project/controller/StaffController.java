package com.project.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.project.model.ApiCaptchaResponse;
import com.project.model.Merchant;
import com.project.model.Staff;
import com.project.service.StaffService;

@CrossOrigin(origins = "*")
@RestController
public class StaffController {

	private static final Logger LOGGER = LoggerFactory.getLogger(StaffController.class);
	
	@Autowired
	StaffService staffService;
	
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*");
            }
        };
    }
	
    @RequestMapping(value = "/testSelect", method = RequestMethod.GET)
    public ResponseEntity<List<Staff>> testSelect() {
        
		LOGGER.info("Memanggil service testSelect ... ");
		List<Staff> liststaff = this.staffService.testSelect();

		return new ResponseEntity<List<Staff>>(liststaff,HttpStatus.OK);
    }
    
	@RequestMapping(value = "/getAllStaffs", method = RequestMethod.POST)
    public ResponseEntity<List<Staff>> getAllStaffs(@RequestBody Staff staff) {
        
		LOGGER.info("Memanggil service getAllStaffs ... ");
		List<Staff> liststaff = this.staffService.getAllStaffs(staff);
		/*for(int i = 0 ; i < liststaff.size(); i++) {
			int counter = i+1;
			LOGGER.info("Staff yang ke " + counter + " untuk merchant " + liststaff.get(i).getFlag() + " adalah " + liststaff.get(i).getNama() + " ... ");
		}*/
		return new ResponseEntity<List<Staff>>(liststaff,HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getStaffDetail", method = RequestMethod.POST)
    public ResponseEntity<Staff> getStaffDetail(@RequestBody Staff staff) {
		
		LOGGER.info("Memanggil service getStaffDetail ... ");
		
		staff = this.staffService.getDetailStaff(staff);
		
		LOGGER.info("Staff dengan id " + staff.getUserId() + " dan bernama " + staff.getNama() + " akan dimunculkan ... ");
		
		return new ResponseEntity<Staff>(staff,HttpStatus.OK);
    }
	
    @RequestMapping(value = "/insertStaff", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> insertStaff(@RequestBody Staff staff) {
        
    	LOGGER.info("Memanggil service insertStaff ...");
    	Integer resultinsert = this.staffService.insertStaff(staff);
        
    	if(resultinsert == 1) { 
    		LOGGER.info("service insertStaff berhasil ...");
    		return new ResponseEntity<Integer>(resultinsert, HttpStatus.CREATED);
    	}
    	else {
    		LOGGER.error("service insertStaff gagal ...");
    		return new ResponseEntity<Integer>(resultinsert, HttpStatus.OK);
    	}
    }
    
    @RequestMapping(value = "/approveMerchant", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Staff> approveMerchant(@RequestBody Staff staff) {
        
    	LOGGER.info("Memanggil service approveMerchant ...");
    	staff = this.staffService.approveMerchant(staff);
        
    	LOGGER.info("Berhasil menjalanakan approveMerchant dengan status -> " + staff.getErrorMessage() + " ...");
		return new ResponseEntity<Staff>(staff, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/deleteAllStaff", method = RequestMethod.POST)
    public Staff deleteAllStaff(@RequestBody Staff staff) {
        return this.staffService.deleteAllStaff(staff);
    }
    
    @RequestMapping(value = "/updateStaff", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> updateStaff(@RequestBody Staff staff) {
    	
    	LOGGER.info("Memanggil service updateStaff ...");
    	Integer resultupdate = this.staffService.updateStaff(staff);
        
    	if(resultupdate == 1) { 
    		LOGGER.info("service updateStaff berhasil ...");
            return new ResponseEntity<Integer>(resultupdate, HttpStatus.OK);
    	}
    	else {
    		LOGGER.error("service updateStaff gagal ...");
            return new ResponseEntity<Integer>(resultupdate, HttpStatus.OK);
    	}
    }
    
    @RequestMapping(value = "/deleteStaffById", method = RequestMethod.POST)
    public ResponseEntity<Integer> deleteStaffById(@RequestBody Staff staff) {
    	LOGGER.info("Memanggil service deleteStaffById ...");
        Integer resultdelete = this.staffService.deleteStaffById(staff);
        
        if(resultdelete == 1) { 
        	LOGGER.info("service deleteStaffById berhasil ...");
            return new ResponseEntity<Integer>(resultdelete, HttpStatus.OK);
        }
    	else { 
    		LOGGER.error("service deleteStaffById gagal ...");
            return new ResponseEntity<Integer>(resultdelete, HttpStatus.OK);
    	}
    }
    
    @RequestMapping(value = "/deleteStaffByIdIsDeleted", method = RequestMethod.POST)
    public ResponseEntity<Integer> deleteStaffByIdIsDeleted(@RequestBody Staff staff) {
    	LOGGER.info("Memanggil service deleteStaffByIdIsDeleted ...");
        Integer resultdelete = this.staffService.deleteStaffByIdIsDeleted(staff);
        
        if(resultdelete == 1) { 
        	LOGGER.info("service deleteStaffByIdIsDeleted berhasil ...");
        	return new ResponseEntity<Integer>(resultdelete, HttpStatus.OK);
        }
    	else {
    		LOGGER.error("service deleteStaffByIdIsDeleted gagal ...");
    		return new ResponseEntity<Integer>(resultdelete, HttpStatus.OK);
    	}
    }
    
    @RequestMapping(value = "/getAllActiveStaffsCount", method = RequestMethod.POST)
    public ResponseEntity<Integer> getAllActiveStaffsCount(@RequestBody Staff staff) {
        
    	LOGGER.info("Memanggil service getAllActiveStaffsCount ...");
    	Integer resultcount = this.staffService.getAllActiveStaffsCount(staff);
        
    	if(resultcount == 0) {
    		LOGGER.error("Tidak ada user yang aktif ...");
    		return new ResponseEntity<Integer>(resultcount, HttpStatus.OK);
    	}
    	else {
    		return new ResponseEntity<Integer>(resultcount, HttpStatus.OK);
    	}
    }
    
    @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public ResponseEntity<Staff> changePassword(@RequestHeader("userId") String userId, @RequestHeader("flag") String flag,
    		@RequestHeader("password") String password, @RequestHeader("passwordBaru") String passwordBaru) {
        
    	LOGGER.info("Memanggil service changePassword ...");
    	Staff staff = this.staffService.changePassword(userId, flag, password, passwordBaru);
        
    	if(!staff.getErrorMessage().equalsIgnoreCase("Update password berhasil")) { 
    		LOGGER.error("Update password gagal ...");
    		return new ResponseEntity<Staff>(staff, HttpStatus.OK);
    	}
    	else { 
    		LOGGER.info("Update password berhasil, password lama -> " + staff.getPassword() + " dan password barunya -> " + staff.getPasswordBaru() + " ...");
    		return new ResponseEntity<Staff>(staff, HttpStatus.OK);
    	}
    }
    
    @RequestMapping(value = "/forgetPassword", method = RequestMethod.POST)
    public ResponseEntity<Staff> forgetPassword(@RequestBody Staff staff) {
        
    	LOGGER.info("Menjalanakan verifikasi captcha ...");
		RestTemplate rest = new RestTemplate();
		final String url = "https://www.google.com/recaptcha/api/siteverify?"
	        		+ "secret=6Ldr8IAUAAAAAInzIfW0k75l7AXMLHieHckiRx8V&response="+ staff.getResponse();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<MultiValueMap<String, String>> headerG = new HttpEntity<>(headers);
		ResponseEntity<ApiCaptchaResponse> responses = rest.postForEntity(url, headerG , ApiCaptchaResponse.class);
		LOGGER.info("Google captcha response controller : "+responses.getBody().getSuccess());
		
		if(responses.getBody().getSuccess().equals("false")) {
			try {
				LOGGER.error("Verifikasi gagal ...");
				throw new Exception("Verifikasi gagal ... ");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		else{
			LOGGER.info("Memanggil service forgetPassword ...");
	    	staff  = this.staffService.forgetPassword(staff);
	        
	    	if(staff.getErrorMessage().equals("Email  / Username tidak sesuai")) {
	    		staff.setPassword(null);
	    		return new ResponseEntity<Staff>(staff, HttpStatus.BAD_REQUEST);
	    	}
	    	else
	        return new ResponseEntity<Staff>(staff, HttpStatus.OK);
		}
		return new ResponseEntity<Staff>(staff, HttpStatus.OK);
		
    }
    
    @RequestMapping(value = "/updateMerchant", method = RequestMethod.GET)
    public ResponseEntity<?> updateMerchant(@RequestHeader("merchantId") String merchantId, @RequestHeader("alamat") String alamat,
    		 @RequestHeader("detaillocation") String detaillocation, @RequestHeader("location") String location, 
    		 @RequestHeader("lat") String lat, @RequestHeader("lng") String lng, @RequestHeader("mobilenumber") String mobilenumber ) {
        
    	LOGGER.info("Memanggil service updateMerchant ...");
    	Staff staff = this.staffService.updateMerchant(merchantId, alamat, detaillocation, location, lat, lng, mobilenumber);
        
    	if(staff.getErrorMessage().equals("Berhasil update merchant")) {
    		return new ResponseEntity<Staff>(staff, HttpStatus.OK);
    	}
    	else {
    		LOGGER.error("Error msg -> " + staff.getErrorMessage());
    		return new ResponseEntity<Staff>(staff, HttpStatus.OK);
    	}
    }
    
    @RequestMapping(value = "/getMerchantForUpdate", method = RequestMethod.GET)
    public ResponseEntity<Merchant> getMerchantForUpdate(@RequestHeader("merchantId") String merchantId) {
        
    	LOGGER.info("Memanggil service getMerchantForUpdate ...");
    	Merchant merchant = this.staffService.getMerchantForUpdate(merchantId);
        
    	if(merchant.getErrorMessage().equals("")) {
    		return new ResponseEntity<Merchant>(merchant, HttpStatus.OK);
    	}
    	else {
    		LOGGER.error("Error msg -> " + merchant.getErrorMessage());
    		return new ResponseEntity<Merchant>(merchant, HttpStatus.OK);
    	}
    }
    
	@RequestMapping(value = "/testRicky", method = RequestMethod.GET)
    public List<Staff> testRicky(Staff staff) {
		
		LOGGER.info("MASUK RICKYYYY ......... ");
		
		return this.staffService.testRicky(staff);
    }
}
