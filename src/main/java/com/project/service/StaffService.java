package com.project.service;

import java.util.List;

import com.project.model.Merchant;
import com.project.model.Staff;

public interface StaffService {

	List<Staff> getAllStaffs(Staff staff);
	Integer getAllActiveStaffsCount(Staff staff);
	Staff getDetailStaff(Staff staff);
	Integer insertStaff (Staff staff);
	Staff deleteAllStaff (Staff staff);
	Integer updateStaff (Staff staff);
	Integer deleteStaffById (Staff staff);
	Integer deleteStaffByIdIsDeleted (Staff staff);
	Staff approveMerchant(Staff staff);
	Staff updateMerchant(String merchantId, String alamat, String detaillocation, String location, String lat, String lng, String mobilenumber);
	Merchant getMerchantForUpdate(String merchantId);
	
	
	Staff changePassword(String userId, String flag, String password, String passwordBaru);
	Staff forgetPassword(Staff staff);
	List<Staff> testRicky(Staff staff);
	List<Staff> testSelect();
	
}