package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.StaffDao;
import com.project.model.Merchant;
import com.project.model.Staff;

@Service
public class StaffServiceImpl implements StaffService{

	@Autowired
	StaffDao staffDao;
	
	@Override
	public List<Staff> getAllStaffs(Staff user) {
		// TODO Auto-generated method stub
		return this.staffDao.getAllStaffs(user);
	}

	@Override
	public Integer insertStaff(Staff user) {
		// TODO Auto-generated method stub
		return this.staffDao.insertStaff(user);
	}

	@Override
	public Staff deleteAllStaff(Staff user) {
		// TODO Auto-generated method stub
		return this.staffDao.deleteAllStaff(user);
	}

	@Override
	public Integer updateStaff(Staff user) {
		// TODO Auto-generated method stub
		return this.staffDao.updateStaff(user);
	}

	@Override
	public Integer deleteStaffById(Staff user) {
		// TODO Auto-generated method stub
		return this.staffDao.deleteStaffById(user);
	}

	@Override
	public List<Staff> testRicky(Staff staff) {
		// TODO Auto-generated method stub
		return this.staffDao.testRicky(staff);
	}

	@Override
	public Staff getDetailStaff(Staff staff) {
		// TODO Auto-generated method stub
		return this.staffDao.getDetailStaff(staff);
	}

	@Override
	public Integer deleteStaffByIdIsDeleted(Staff staff) {
		// TODO Auto-generated method stub
		return this.staffDao.deleteStaffByIdIsDeleted(staff);
	}

	@Override
	public Integer getAllActiveStaffsCount(Staff staff) {
		// TODO Auto-generated method stub
		return this.staffDao.getAllActiveStaffsCount(staff);
	}

	@Override
	public Staff changePassword(String userId, String flag, String password, String passwordBaru) {
		// TODO Auto-generated method stub
		return this.staffDao.changePassword(userId, flag, password, passwordBaru);
	}

	@Override
	public Staff forgetPassword(Staff staff) {
		// TODO Auto-generated method stub
		return this.staffDao.forgetPassword(staff);
	}

	@Override
	public Staff approveMerchant(Staff staff) {
		// TODO Auto-generated method stub
		return this.staffDao.approveMerchant(staff);
	}

	@Override
	public Staff updateMerchant(String merchantId, String alamat, String detaillocation, String location, String lat,
			String lng, String mobilenumber) {
		// TODO Auto-generated method stub
		return this.staffDao.updateMerchant(merchantId, alamat, detaillocation, location, lat, lng, mobilenumber);
	}

	@Override
	public Merchant getMerchantForUpdate(String merchantId) {
		// TODO Auto-generated method stub
		return this.staffDao.getMerchantForUpdate(merchantId);
	}

	@Override
	public List<Staff> testSelect() {
		// TODO Auto-generated method stub
		return this.staffDao.testSelect();
	}

/*	@Override
	public List<Staff> checkEmailExist() {
		// TODO Auto-generated method stub
		return this.staffDao.checkEmailExist();
	}*/

}
