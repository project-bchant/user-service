package com.project.dbconn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class getDbConnection {

	public static Connection Access(){
        try {
        	Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
            		"jdbc:oracle:thin:@10.1.125.198:1521:XE", 
            		"test", 
            		"root");
        } catch (SQLException e) {
        	e.printStackTrace();
            return null;
        }
        
        return connection;
	}
}
