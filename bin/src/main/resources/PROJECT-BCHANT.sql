BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE userr';
EXCEPTION
  WHEN OTHERS THEN
    NULL;
END;

BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE merchant';
EXCEPTION
  WHEN OTHERS THEN
    NULL;
END;

BEGIN
	EXECUTE IMMEDIATE 'DROP SEQUENCE USER_SEQ';
EXCEPTION
	WHEN OTHERS THEN
		NULL;
END; 

create table Userr (
    UserID varchar2(50) NOT NUll,
    Nama varchar2(50) NOT NULL,
    UserPassword varchar2(50) NOT NULL,
    Email varchar2(50) NOT NULL,
    MerchantID varchar2(50) NOT NULL,
    MenuAccess varchar2(100) NOT NUll,
    CONSTRAINT userr_pk primary key (userID)
);

create table Merchant (
    MerchantID varchar2(50) NOT NULL,
    NamaMerchant varchar2(50) not null,
    NamaOwner varchar2(50) not null,
    RekMerchant numeric(10, 0) not null,
    Email varchar2(50) not null,
    NoTelp numeric(12,0) not null,
    Alamat varchar2(100) not null,
    Status varchar2(50) not null,
    constraint merchant_pk primary key (merchantid)
);

CREATE SEQUENCE user_seq
MINVALUE 1
START WITH 1
INCREMENT BY 1 NOCACHE NOCYCLE;

insert into userr values (user_seq.nextval,'Ian','1','ian@gmail.com','1','Menu 1,Menu 2,Menu 3','uian1','0','1');
insert into userr values (user_seq.nextval,'Ian2','12','ian2@gmail.com','1','Menu 1', 'uian2','0','0');
insert into userr values (user_seq.nextval,'Ian3','123','ian3@gmail.com','1','Menu 1,Menu 2','uian3','0','0');
insert into userr values (user_seq.nextval,'Ian4','1234','ian4@gmail.com','1','Menu 1,Menu 2,Menu 3','uian4','0','0');
insert into userr values (user_seq.nextval,'Ian5','12345','ian5@gmail.com','1','Menu 1,Menu 2,Menu 3','uian5','0','0');

select * from userr where nama like '%%';

variable nama varchar2(50);
variable outt refcursor;

execute get_all_users_by_name('2','M0', :outt);

print outt;

delete from userr;


variable p_id varchar(50),
variable p_nama varchar(50),
variable p_password varchar(50),
variable p_email varchar(50),
variable p_merchantid varchar(50),
variable p_menuaccess varchar(50)

execute insert_user(user_seq.nextval, 'Ian6', '123456', 'Ian6@gmail.com', 'M01', 'Insert6');


