create or replace PROCEDURE GET_ALL_USERS
(
	p_flag in userr.flag%TYPE,
    result out SYS_REFCURSOR
)
AS 
BEGIN
    OPEN RESULT FOR
      SELECT u.userid, u.nama, u.email, u.menuaccess, u.flag, u.username, m.merchantname
      FROM USERR u, merchant m
      WHERE m.merchantid = u.flag and u.flag = p_flag and u.isdeleted = '0'
      ORDER BY u.USERID ASC;
END GET_ALL_USERS;

create or replace PROCEDURE GET_ALL_USER_EMAIL_USERNAME
(
    result out SYS_REFCURSOR
)
AS 
BEGIN
  OPEN RESULT FOR
    SELECT EMAIL, USERNAME
    FROM USERR;
END GET_ALL_USER_EMAIL_USERNAME;

create or replace PROCEDURE GET_LAST_FLAG
(
    result out SYS_REFCURSOR
)
AS 
BEGIN
  OPEN RESULT FOR
    SELECT merchantid 
    FROM merchant
    ORDER BY merchantid DESC;
END GET_LAST_FLAG;

create or replace PROCEDURE GET_USER_DETAIL
(
    p_id in userr.userid%TYPE,
    p_flag in userr.flag%TYPE,
    result out SYS_REFCURSOR
)
AS 
BEGIN
  OPEN RESULT FOR
      SELECT u.userid, u.nama, u.email, u.menuaccess, u.flag, u.username, m.merchantname
      FROM USERR u, merchant m
      WHERE m.merchantid = u.flag and
      u.USERID = p_id and u.flag = p_flag;
END GET_USER_DETAIL;

create or replace PROCEDURE INSERT_USER
(
    p_nama in userr.nama%TYPE,
    p_password in userr.userpassword%TYPE,
    p_email in userr.email%TYPE,
    p_flag in userr.flag%TYPE,
    p_menuaccess in userr.menuaccess%TYPE,
    p_username in userr.username%TYPE,
    result out sys_refcursor
)
AS 
v_count_username number;
v_count_email number;
v_error_msg varchar2(100) := '';
v_error_msg_numb number;
BEGIN

    select count(username) into v_count_username from userr where username = p_username;
    select count(email) into v_count_email from userr where email = p_email;

    if ( v_count_username = 0 and v_count_email = 0) THEN  
        insert into userr (userid, nama, userpassword, email, flag, menuaccess, username, ISDELETED, isOwner)
        values (user_seq.nextval, p_nama, p_password, p_email, p_flag, p_menuaccess, p_username, '0', '0');
        v_error_msg_numb := 1;
            else if(v_count_username != 0 and v_count_email = 0) THEN
                v_error_msg := 'Username sudah terdaftar';
                v_error_msg_numb := -1;
                else if(v_count_username = 0 and v_count_email != 0) THEN
                    v_error_msg := 'Email sudah terdaftar';
                    v_error_msg_numb := 0;
                    else
                        v_error_msg := 'Email dan Username sudah terdaftar';
                        v_error_msg_numb := -2;
                end if;
            end if;
    end if;
    
    open result for
        select v_error_msg as v_error_msg,
                v_error_msg_numb as v_error_msg_numb
        from dual;
    
  commit;
END INSERT_USER;

create or replace PROCEDURE UPDATE_USER
(
    p_id in userr.userid%TYPE,
    p_nama in userr.nama%TYPE,
    p_flag in USERR.flag%TYPE,
    p_menuaccess in USERR.MENUACCESS%TYPE,
    p_email in userr.email%TYPE,
    result out sys_refcursor
)
AS 
v_is_update number;
v_email_lama varchar2(100);
v_counter_email number;
v_error_msg varchar2(100) := '';
v_error_msg_numb number;
BEGIN
  select email into v_email_lama from userr where userid = p_id;
  
  if(v_email_lama = p_email) then
    UPDATE USERR SET NAMA = P_NAMA, menuaccess = p_menuaccess
    WHERE flag = p_flag and userid = p_id;
    v_is_update := SQL%ROWCOUNT;
              
              if(v_is_update = 0) THEN
                  v_error_msg := 'User tidak ditemukan';
                  v_error_msg_numb := 0;
                  else if(v_is_update = 1) THEN
                      v_error_msg := 'Update user berhasil';
                      v_error_msg_numb := 1;
                      else
                          v_error_msg := 'Kemungkinan ID Ganda, Silahkan dicek stored procedure UPDATE_USER dan DB User';
                          v_error_msg_numb := 0;
                  end if;
              end if;
    else
        select count(email) into v_counter_email from userr where email = p_email;
            if(v_counter_email = 0) THEN
              UPDATE USERR SET NAMA = P_NAMA, menuaccess = p_menuaccess, email = p_email
              WHERE flag = p_flag and userid = p_id;
              
              v_is_update := SQL%ROWCOUNT;
              
              if(v_is_update = 0) THEN
                  v_error_msg := 'User tidak ditemukan';
                  v_error_msg_numb := 0;
                  else if(v_is_update = 1) THEN
                      v_error_msg := 'Update user berhasil';
                      v_error_msg_numb := 1;
                      else
                          v_error_msg := 'Kemungkinan ID Ganda, Silahkan dicek stored procedure UPDATE_USER dan DB User';
                          v_error_msg_numb := 0;
                  end if;
              end if;
          else 
            v_error_msg := 'Email sudah terdaftar';
            v_error_msg_numb := 0;
          end if;
  end if;

  open result for
    select v_is_update as updated_row_count,
        v_error_msg as v_error_msg,
    v_error_msg_numb as v_error_msg_numb
    from dual;
  COMMIT;
END UPDATE_USER;

create or replace PROCEDURE DELETE_USER_BY_ID
(
    p_id in userr.userid%TYPE,
    p_flag in userr.flag%TYPE,
    result out SYS_REFCURSOR
)
AS 
v_is_delete number;
BEGIN
  DELETE FROM USERR
  WHERE USERID = P_ID AND flag = p_flag;
  
   v_is_delete := SQL%ROWCOUNT;
  
  open result for
    select v_is_delete as deleted_row_count
    from dual;
  COMMIT;

END DELETE_USER_BY_ID;

create or replace PROCEDURE DELETE_ALL_USER 
(
    p_flag in userr.flag%TYPE
)
AS 
BEGIN
  delete from userr
  where flag = p_flag;
  commit;
END DELETE_ALL_USER;

create or replace PROCEDURE CHANGE_PASSWORD
(
    p_id in userr.userid%TYPE,
    p_flag in userr.flag%TYPE,
    p_passwordbaru in USERR.USERPASSWORD%TYPE,
    result out sys_refcursor
)
AS
    v_is_update number;
    v_count_password number;
    v_error_msg varchar2(100) := '';
    v_error_msg_numb number;
BEGIN

      update userr set userpassword = p_passwordbaru 
      where userid = p_id and flag = p_flag;
      v_is_update := SQL%ROWCOUNT;
        if(v_is_update = 0) THEN
            v_error_msg := 'Update password gagal, userId tidak ditemukan';
            v_error_msg_numb := 0;        
        else
          v_error_msg_numb := 1;
          v_error_msg := 'Update password berhasil';
        end if;

  open result for
    select v_is_update as updated_row_count,
    v_error_msg as v_error_msg,
    v_error_msg_numb as v_error_msg_numb
    from dual;
  COMMIT;
END CHANGE_PASSWORD;

create or replace PROCEDURE FORGET_PASSWORD 
(
    p_id in userr.userid%TYPE,
    p_flag in userr.flag%TYPE,
    p_password in USERR.USERPASSWORD%TYPE,
    result out sys_refcursor
)
AS
    v_is_update number;
BEGIN
  update userr set userpassword = p_password 
  where userid = p_id and flag = p_flag;
  
  v_is_update := SQL%ROWCOUNT;
  
  open result for
    select v_is_update as updated_row_count
    from dual;
  COMMIT;
END FORGET_PASSWORD;

create or replace PROCEDURE UPDATE_USER_FOR_DELETE
(
    p_id in userr.userid%TYPE,
    p_flag in userr.flag%Type,
    result out sys_refcursor
)
AS 
v_is_update_delete number;
BEGIN
  UPDATE USERR SET ISDELETED = '1' where flag = p_flag and userid = p_id;
  
  v_is_update_delete := SQL%ROWCOUNT;
  
  open result for
    select v_is_update_delete as updated_row_count
    from dual;
  COMMIT;
END UPDATE_USER_FOR_DELETE;

create or replace PROCEDURE APPROVE_MERCHANT
(
    p_status in merchant.status%TYPE,
    p_merchantid in merchant.merchantid%TYPE,
    p_nama in userr.nama%TYPE,
    p_password in userr.userpassword%TYPE,
    p_email in userr.email%TYPE,
    p_username in userr.username%TYPE,
    result out sys_refcursor
)
AS 
v_is_update number;
v_err_msg varchar2(100);
v_err_msg_numb number;
v_menu_access varchar2(100) := 'Menu 1,Menu 2,Menu 3';
BEGIN
  UPDATE merchant SET status = p_status where merchantid = p_merchantid;
  
  v_is_update := SQL%ROWCOUNT;
  
  if(v_is_update = 0) THEN
    v_err_msg := 'Update status merchant gagal, MerchantId tidak ditemukan';
    v_err_msg_numb := 0;
    else if(v_is_update = 1) then
        v_err_msg := 'Status merchant berhasil di update';
            if(p_status = 'Approved') then
                insert into userr (userid, nama, userpassword, email, flag, menuaccess, username, ISDELETED, isOwner)
                values (user_seq.nextval, p_nama, p_password, p_email, p_merchantid, v_menu_access, p_username, '0', '1');
                v_err_msg_numb := 1;
                v_err_msg := 'Merchant berhasil di approved, User Merchant sudah aktif';
            else
                v_err_msg:= 'Status merchant di rejected';
                v_err_msg_numb := -1;
            end if;
    end if;
  end if;
  
  open result for 
    select v_is_update as v_is_update,
           v_err_msg as v_err_msg,
           v_err_msg_numb as v_err_msg_numb
           FROM DUAL;

    COMMIT;
END APPROVE_MERCHANT;

create or replace PROCEDURE GET_ALL_USERS_BY_NAME
(
    p_name in userr.nama%TYPE,
    p_flag in userr.flag%TYPE,
    result out SYS_REFCURSOR
)
AS 
BEGIN
  OPEN RESULT FOR
    SELECT *
    FROM USERR
    WHERE NAMA LIKE '%' || p_name ||'%'
    AND flag = p_flag;
END GET_ALL_USERS_BY_NAME;


CREATE SEQUENCE  "TEST"."USER_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 8 NOCACHE  NOORDER  NOCYCLE ;