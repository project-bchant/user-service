--------------------------------------------------------
--  File created - Thursday-November-29-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CUSTOMER
--------------------------------------------------------

  CREATE TABLE "TEST"."CUSTOMER" 
   (	"CUSTOMERID" NUMBER(*,0), 
	"CUSTOMERNAME" VARCHAR2(255 BYTE), 
	"PASSWORD" VARCHAR2(255 BYTE), 
	"SALDO" NUMBER, 
	"EMAIL" VARCHAR2(255 BYTE), 
	"PIN" VARCHAR2(255 BYTE), 
	"USERNAME" VARCHAR2(255 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table EMPLOYEE
--------------------------------------------------------

  CREATE TABLE "TEST"."EMPLOYEE" 
   (	"EMPID" VARCHAR2(10 BYTE), 
	"EMPNAME" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table MERCHANT
--------------------------------------------------------

  CREATE TABLE "TEST"."MERCHANT" 
   (	"MERCHANTID" NUMBER(19,0), 
	"ADDRESS" VARCHAR2(255 CHAR), 
	"DETAILLOCATION" VARCHAR2(255 CHAR), 
	"EMAIL" VARCHAR2(255 CHAR), 
	"LOCATION" VARCHAR2(255 CHAR), 
	"MERCHANTNAME" VARCHAR2(255 CHAR), 
	"MOBILENUMBER" VARCHAR2(255 CHAR), 
	"OWNERNAME" VARCHAR2(255 CHAR), 
	"REKMERCHANT" VARCHAR2(255 CHAR), 
	"STATUS" VARCHAR2(255 CHAR), 
	"USERNAME" VARCHAR2(255 CHAR), 
	"DESCRIPTION" VARCHAR2(30 BYTE), 
	"CREATEDAT" TIMESTAMP (6), 
	"LAT" VARCHAR2(255 CHAR), 
	"LNG" VARCHAR2(255 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table MERCHANT_OLD
--------------------------------------------------------

  CREATE TABLE "TEST"."MERCHANT_OLD" 
   (	"MERCHANTNAME" VARCHAR2(50 BYTE), 
	"OWNERNAME" VARCHAR2(50 BYTE), 
	"EMAIL" VARCHAR2(50 BYTE), 
	"ADDRESS" VARCHAR2(100 BYTE), 
	"STATUS" VARCHAR2(50 BYTE), 
	"USERNAME" VARCHAR2(30 BYTE), 
	"LOCATION" VARCHAR2(30 BYTE), 
	"DETAILLOCATION" VARCHAR2(30 BYTE), 
	"MOBILENUMBER" VARCHAR2(20 BYTE), 
	"REKMERCHANT" VARCHAR2(20 BYTE), 
	"MERCHANTID" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table PRODUCT
--------------------------------------------------------

  CREATE TABLE "TEST"."PRODUCT" 
   (	"PRODUCTID" VARCHAR2(255 BYTE), 
	"NAMA" VARCHAR2(255 BYTE), 
	"QTY" NUMBER, 
	"HARGA" VARCHAR2(255 BYTE), 
	"KODE" VARCHAR2(255 BYTE), 
	"MERCHANTID" NUMBER(19,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table SALES
--------------------------------------------------------

  CREATE TABLE "TEST"."SALES" 
   (	"SALESID" NUMBER(*,0), 
	"TRXID" NUMBER(*,0), 
	"MERCHANTID" NUMBER(*,0), 
	"CUSTOMERID" NUMBER(*,0), 
	"TOTAL" NUMBER, 
	"PRODUCTID" VARCHAR2(255 BYTE), 
	"QTY" NUMBER(*,0), 
	"HARGAST" NUMBER, 
	"TGL" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table USERR
--------------------------------------------------------

  CREATE TABLE "TEST"."USERR" 
   (	"USERID" VARCHAR2(50 BYTE), 
	"NAMA" VARCHAR2(50 BYTE), 
	"USERPASSWORD" VARCHAR2(100 BYTE), 
	"EMAIL" VARCHAR2(50 BYTE), 
	"FLAG" VARCHAR2(50 BYTE), 
	"MENUACCESS" VARCHAR2(100 BYTE), 
	"USERNAME" VARCHAR2(50 BYTE), 
	"ISDELETED" VARCHAR2(10 BYTE), 
	"ISOWNER" VARCHAR2(10 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Sequence MERCHANT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "TEST"."MERCHANT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 61 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence USER_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "TEST"."USER_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 36 NOCACHE  NOORDER  NOCYCLE ;
REM INSERTING into TEST.CUSTOMER
SET DEFINE OFF;
Insert into TEST.CUSTOMER (CUSTOMERID,CUSTOMERNAME,PASSWORD,SALDO,EMAIL,PIN,USERNAME) values (1,'Glenn Raditya','glen123',5000000,'glenn@gmail.com','123123','ggr');
Insert into TEST.CUSTOMER (CUSTOMERID,CUSTOMERNAME,PASSWORD,SALDO,EMAIL,PIN,USERNAME) values (3,'Daniel Christiawan','dan123',9000000,'dc@gmail.com','123123','dcn');
REM INSERTING into TEST.EMPLOYEE
SET DEFINE OFF;
Insert into TEST.EMPLOYEE (EMPID,EMPNAME) values ('emp5','testInsert');
Insert into TEST.EMPLOYEE (EMPID,EMPNAME) values ('emp5','testInsert');
Insert into TEST.EMPLOYEE (EMPID,EMPNAME) values ('emp5','testInsert');
Insert into TEST.EMPLOYEE (EMPID,EMPNAME) values ('emp5','testInsert');
Insert into TEST.EMPLOYEE (EMPID,EMPNAME) values ('emp5','testInsert');
Insert into TEST.EMPLOYEE (EMPID,EMPNAME) values ('emp2','asd');
Insert into TEST.EMPLOYEE (EMPID,EMPNAME) values ('emp2','asd');
REM INSERTING into TEST.MERCHANT
SET DEFINE OFF;
Insert into TEST.MERCHANT (MERCHANTID,ADDRESS,DETAILLOCATION,EMAIL,LOCATION,MERCHANTNAME,MOBILENUMBER,OWNERNAME,REKMERCHANT,STATUS,USERNAME,DESCRIPTION,CREATEDAT,LAT,LNG) values (1,'UMN',null,'ricky.surya@student.umn.ac.id',null,'Ricky Merchant',null,'ricky',null,'Approved',null,null,to_timestamp('28-NOV-18 03.12.08.064000000 PM','DD-MON-RR HH.MI.SSXFF AM'),null,null);
Insert into TEST.MERCHANT (MERCHANTID,ADDRESS,DETAILLOCATION,EMAIL,LOCATION,MERCHANTNAME,MOBILENUMBER,OWNERNAME,REKMERCHANT,STATUS,USERNAME,DESCRIPTION,CREATEDAT,LAT,LNG) values (52,'jakarta','sandang','jefry@jefry.com','jakarta barat','fish streat','08123456789','Kevin','5213578901','Approved','jefry123',null,to_timestamp('28-NOV-18 03.26.58.921000000 PM','DD-MON-RR HH.MI.SSXFF AM'),null,null);
Insert into TEST.MERCHANT (MERCHANTID,ADDRESS,DETAILLOCATION,EMAIL,LOCATION,MERCHANTNAME,MOBILENUMBER,OWNERNAME,REKMERCHANT,STATUS,USERNAME,DESCRIPTION,CREATEDAT,LAT,LNG) values (53,'jakarta','sandang','jefry@jefry.com','jakarta barat','fish streat','08123456789','Kevin','5213578901','Waiting','jefry123',null,to_timestamp('28-NOV-18 03.27.46.510000000 PM','DD-MON-RR HH.MI.SSXFF AM'),null,null);
Insert into TEST.MERCHANT (MERCHANTID,ADDRESS,DETAILLOCATION,EMAIL,LOCATION,MERCHANTNAME,MOBILENUMBER,OWNERNAME,REKMERCHANT,STATUS,USERNAME,DESCRIPTION,CREATEDAT,LAT,LNG) values (54,'jakarta','sandang','jefry@jefry.com','jakarta barat','fish streat','08123456789','Kevin','5213578901','Waiting','jefry123',null,to_timestamp('28-NOV-18 03.28.14.027000000 PM','DD-MON-RR HH.MI.SSXFF AM'),null,null);
Insert into TEST.MERCHANT (MERCHANTID,ADDRESS,DETAILLOCATION,EMAIL,LOCATION,MERCHANTNAME,MOBILENUMBER,OWNERNAME,REKMERCHANT,STATUS,USERNAME,DESCRIPTION,CREATEDAT,LAT,LNG) values (55,'jakarta','sandang','jefry@jefry.com','jakarta barat','fish streat','08123456789','Kevin','5213578901','Waiting','jefry123',null,to_timestamp('28-NOV-18 03.30.29.692000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'-6.232804199999999','106.8563641');
Insert into TEST.MERCHANT (MERCHANTID,ADDRESS,DETAILLOCATION,EMAIL,LOCATION,MERCHANTNAME,MOBILENUMBER,OWNERNAME,REKMERCHANT,STATUS,USERNAME,DESCRIPTION,CREATEDAT,LAT,LNG) values (56,'jakarta','petojo','kevin@kevin.com','jakarta barat','Kevin goreng','08123456789','Kevin','5213578901','Waiting','kevin123',null,to_timestamp('29-NOV-18 10.59.29.003000000 AM','DD-MON-RR HH.MI.SSXFF AM'),'-6.232804199999999',null);
Insert into TEST.MERCHANT (MERCHANTID,ADDRESS,DETAILLOCATION,EMAIL,LOCATION,MERCHANTNAME,MOBILENUMBER,OWNERNAME,REKMERCHANT,STATUS,USERNAME,DESCRIPTION,CREATEDAT,LAT,LNG) values (4,'UUMN','serpong','icon@gmail.com','tangerang selatan','icon shop','08123456789','Icon','2213214123','Approved','icon123',null,to_timestamp('28-NOV-18 03.12.08.064000000 PM','DD-MON-RR HH.MI.SSXFF AM'),null,null);
Insert into TEST.MERCHANT (MERCHANTID,ADDRESS,DETAILLOCATION,EMAIL,LOCATION,MERCHANTNAME,MOBILENUMBER,OWNERNAME,REKMERCHANT,STATUS,USERNAME,DESCRIPTION,CREATEDAT,LAT,LNG) values (51,'jakarta','sandang','jefry@jefry.com','jakarta barat','kevin Shop','08123456789','Kevin','5213578901','Waiting','jefry123',null,to_timestamp('28-NOV-18 03.12.08.064000000 PM','DD-MON-RR HH.MI.SSXFF AM'),null,null);
REM INSERTING into TEST.MERCHANT_OLD
SET DEFINE OFF;
Insert into TEST.MERCHANT_OLD (MERCHANTNAME,OWNERNAME,EMAIL,ADDRESS,STATUS,USERNAME,LOCATION,DETAILLOCATION,MOBILENUMBER,REKMERCHANT,MERCHANTID) values ('Ricky Merchant','Ricky','ricky.surya@student.umn.ac.id','UMN','Approved',null,null,null,null,null,'1');
REM INSERTING into TEST.PRODUCT
SET DEFINE OFF;
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('FD2','Nasi Tim',2,'40000','NT',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('ICN2','Nasi Goreng by icon editv2',3,'12000','ngbicn',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('FD3','Nasi Uduk',5,'26000','NU',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('FD4','Mie Rebus',3,'18000','MR',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('DR1','Es Teh',9,'7000','ET',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('DR2','Jeruk Hangat',3,'12000','JH',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('DR3','Jus Apel',7,'11000','JA',3);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('DR4','Jus Pepaya',2,'8000','JP',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('DR5','Susu Coklat',6,'10000','SC',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('FD5','Nasi Gudeg',6,'31000','NGU',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('MK2','Bubur Ayam',1,'10000','BA',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('MK4','Indomie Goreng',1,'10000','IM',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('ICN3','Nasi goreng by icon 23',33,'120003','ngbicn3',13);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('MK3','Kerupuk',1,'5500','KR',1);
Insert into TEST.PRODUCT (PRODUCTID,NAMA,QTY,HARGA,KODE,MERCHANTID) values ('MK1','Mie Goreng',1,'11000','MG',1);
REM INSERTING into TEST.SALES
SET DEFINE OFF;
Insert into TEST.SALES (SALESID,TRXID,MERCHANTID,CUSTOMERID,TOTAL,PRODUCTID,QTY,HARGAST,TGL) values (3,2,1,1,10000,'FD3',4,2500,to_date('19-NOV-18','DD-MON-RR'));
Insert into TEST.SALES (SALESID,TRXID,MERCHANTID,CUSTOMERID,TOTAL,PRODUCTID,QTY,HARGAST,TGL) values (1,1,1,1,50000,'FD2',2,12000,to_date('20-NOV-18','DD-MON-RR'));
Insert into TEST.SALES (SALESID,TRXID,MERCHANTID,CUSTOMERID,TOTAL,PRODUCTID,QTY,HARGAST,TGL) values (4,2,1,1,10000,'FD4',4,2500,to_date('19-NOV-18','DD-MON-RR'));
Insert into TEST.SALES (SALESID,TRXID,MERCHANTID,CUSTOMERID,TOTAL,PRODUCTID,QTY,HARGAST,TGL) values (2,4,1,3,25000,'FD3',10,2500,to_date('21-NOV-18','DD-MON-RR'));
Insert into TEST.SALES (SALESID,TRXID,MERCHANTID,CUSTOMERID,TOTAL,PRODUCTID,QTY,HARGAST,TGL) values (5,4,1,3,25000,'FD5',10,2500,to_date('21-NOV-18','DD-MON-RR'));
REM INSERTING into TEST.USERR
SET DEFINE OFF;
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('8','Ian','1','ian@gmail.com','1','Menu 1,Menu 2,Menu 3','uian1','0','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('9','Ian2','12','ian2@gmail.com','1','Menu 1','uian2','0','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('10','Ian3','123','ian3@gmail.com','1','Menu 1,Menu 3','uian3','0','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('11','Ian4','1234','ian4@gmail.com','1','Menu 1,Menu 2,Menu 3','uian4','0','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('14','Toko Online Ricky','$2a$10$MbQSe.c0pQ7jNmfMHnvbDOMccGGAMNESz198MNFHu6yA.kgL.RYJi','ricky.surya@student.umn.ac.id','1','Menu 1,Menu 2,Menu 3','ricky123','0','1');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('12','Ian5','12345','ian5@gmail.com','1','Menu 1,Menu 2,Menu 3','uian5','0','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('13','ricky surya','$2a$10$nK6zPD3qeMfIp.scxMdbHeAzqycUsO4JX0Wt9lP3WcitQ/7QFAx0G','ricky@gmail.com','1','Menu 1,Menu 3','ricky01','1','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('15','testRicky','$2a$10$Cx4OYCiMivKZnE7rGojeMune6iSe6mb7ANEWefGen3syb3BrET6RO','test@test.com','1','Menu 2','test','0','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('16','test','$2a$10$RXH4amJFigcBD3s3mUWJfOprUiISFULcJg8FZ4KPgDxF6fn2oyasG','test@gmail.com','1','Menu 3','test2','0','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('17','Ian Email Insert','$2a$10$PvG4s3P.PYiCN.wlvhxnLuJV2u8v7WYQgPTzT3ZGETxulR1jW9za.','joshuaiann@gmail.com','1','Menu 1,Menu 2,Menu 3','ian123','0','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('35','Hari Kamis','$2a$10$O6BNYQBRSrpQn2qd07/dTeVEVQKDbKaeqgYfnPs2TSfP030p4hGm2','jumat@test.co.id','1','Menu 2,Menu 3','test29','1','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('33','Kevin','$2a$10$Bi9WSrxHt9PRwFgcsTcMdOXdJN6JON4HHgMOuFKYhiEWt1TzQlImi','jefry@jefry.com','52','Menu 1,Menu 2,Menu 3','jefry123','0','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('34','Christian Wilyawan','$2a$10$iyVW3gdBYjx6QrQTJQGJU.7JdQLmWD.YziVRQK8z2PQqVzC387E1C','wilyawan.christian@gmail.com','1','Menu 1, Menu 2','ctw69','0','0');
Insert into TEST.USERR (USERID,NAMA,USERPASSWORD,EMAIL,FLAG,MENUACCESS,USERNAME,ISDELETED,ISOWNER) values ('28','aaa','$2a$10$UWfD0.TdhHFCdi9m1XWPZeFb7dlnm26GgVJkxGs7n.vK8UcpzqUmu','aaa','1','Menu 1','aaa','1','0');
--------------------------------------------------------
--  DDL for Index USERR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TEST"."USERR_PK" ON "TEST"."USERR" ("USERID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PRODUCT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TEST"."PRODUCT_PK" ON "TEST"."PRODUCT" ("PRODUCTID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index MERCHANT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TEST"."MERCHANT_PK" ON "TEST"."MERCHANT_OLD" ("MERCHANTID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index CUSTOMER_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TEST"."CUSTOMER_PK" ON "TEST"."CUSTOMER" ("CUSTOMERID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index MERCHANTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TEST"."MERCHANTS_PK" ON "TEST"."MERCHANT" ("MERCHANTID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index SALES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TEST"."SALES_PK" ON "TEST"."SALES" ("SALESID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Procedure APPROVE_MERCHANT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."APPROVE_MERCHANT" 
(
    p_status in merchant.status%TYPE,
    p_merchantid in merchant.merchantid%TYPE,
    p_nama in userr.nama%TYPE,
    p_password in userr.userpassword%TYPE,
    p_email in userr.email%TYPE,
    p_username in userr.username%TYPE,
    result out sys_refcursor
)
AS 
v_is_update number;
v_err_msg varchar2(100);
v_err_msg_numb number;
v_menu_access varchar2(100) := 'Menu 1,Menu 2,Menu 3';
BEGIN
  UPDATE merchant SET status = p_status where merchantid = p_merchantid;
  
  v_is_update := SQL%ROWCOUNT;
  
  if(v_is_update = 0) THEN
    v_err_msg := 'Update status merchant gagal, MerchantId tidak ditemukan';
    v_err_msg_numb := 0;
    else if(v_is_update = 1) then
        v_err_msg := 'Status merchant berhasil di update';
            if(p_status = 'Approved') then
                insert into userr (userid, nama, userpassword, email, flag, menuaccess, username, ISDELETED, isOwner)
                values (user_seq.nextval, p_nama, p_password, p_email, p_merchantid, v_menu_access, p_username, '0', '1');
                v_err_msg_numb := 1;
                v_err_msg := 'Merchant berhasil di approved, User Merchant sudah aktif';
            else
                v_err_msg:= 'Status merchant di rejected';
                v_err_msg_numb := -1;
            end if;
    end if;
  end if;
  
  open result for 
    select v_is_update as v_is_update,
           v_err_msg as v_err_msg,
           v_err_msg_numb as v_err_msg_numb
           FROM DUAL;

    COMMIT;
END APPROVE_MERCHANT;

/
--------------------------------------------------------
--  DDL for Procedure CHANGE_PASSWORD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."CHANGE_PASSWORD" 
(
    p_id in userr.userid%TYPE,
    p_flag in userr.flag%TYPE,
    p_passwordbaru in USERR.USERPASSWORD%TYPE,
    result out sys_refcursor
)
AS
    v_is_update number;
    v_count_password number;
    v_error_msg varchar2(100) := '';
    v_error_msg_numb number;
BEGIN

      update userr set userpassword = p_passwordbaru 
      where userid = p_id and flag = p_flag;
      v_is_update := SQL%ROWCOUNT;
        if(v_is_update = 0) THEN
            v_error_msg := 'Update password gagal, userId tidak ditemukan';
            v_error_msg_numb := 0;        
        else
          v_error_msg_numb := 1;
          v_error_msg := 'Update password berhasil';
        end if;

  open result for
    select v_is_update as updated_row_count,
    v_error_msg as v_error_msg,
    v_error_msg_numb as v_error_msg_numb
    from dual;
  COMMIT;
END CHANGE_PASSWORD;

/
--------------------------------------------------------
--  DDL for Procedure DELETE_ALL_USER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."DELETE_ALL_USER" 
(
    p_flag in userr.flag%TYPE
)
AS 
BEGIN
  delete from userr
  where flag = p_flag;
  commit;
END DELETE_ALL_USER;

/
--------------------------------------------------------
--  DDL for Procedure DELETE_USER_BY_ID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."DELETE_USER_BY_ID" 
(
    p_id in userr.userid%TYPE,
    p_flag in userr.flag%TYPE,
    result out SYS_REFCURSOR
)
AS 
v_is_delete number;
BEGIN
  DELETE FROM USERR
  WHERE USERID = P_ID AND flag = p_flag;
  
   v_is_delete := SQL%ROWCOUNT;
  
  open result for
    select v_is_delete as deleted_row_count
    from dual;
  COMMIT;

END DELETE_USER_BY_ID;

/
--------------------------------------------------------
--  DDL for Procedure FORGET_PASSWORD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."FORGET_PASSWORD" 
(
    p_id in userr.userid%TYPE,
    p_flag in userr.flag%TYPE,
    p_password in USERR.USERPASSWORD%TYPE,
    result out sys_refcursor
)
AS
    v_is_update number;
BEGIN
  update userr set userpassword = p_password 
  where userid = p_id and flag = p_flag;
  
  v_is_update := SQL%ROWCOUNT;
  
  open result for
    select v_is_update as updated_row_count
    from dual;
  COMMIT;
END FORGET_PASSWORD;

/
--------------------------------------------------------
--  DDL for Procedure GET_ALL_ACTIVE_USERS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."GET_ALL_ACTIVE_USERS" 
(
	p_flag in userr.flag%TYPE,
    result out SYS_REFCURSOR
)
AS 
BEGIN
   OPEN RESULT FOR
      SELECT count(u.username) as jumlah
      FROM USERR u, merchant m
      WHERE m.merchantid = u.flag and u.flag = p_flag and u.ISDELETED = '0'
      ORDER BY u.USERID ASC;
END GET_ALL_ACTIVE_USERS;

/
--------------------------------------------------------
--  DDL for Procedure GET_ALL_PRODUCTS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."GET_ALL_PRODUCTS" 
(
    p_id_merchant    IN  product.merchantid%TYPE,
    result           OUT SYS_REFCURSOR
)
AS 
BEGIN
    OPEN RESULT FOR
      SELECT productid, nama, qty, harga, kode, merchantid
      FROM product
      WHERE merchantid = p_id_merchant
      ORDER BY productid asc;
END GET_ALL_PRODUCTS;

/
--------------------------------------------------------
--  DDL for Procedure GET_ALL_USERS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."GET_ALL_USERS" 
(
	p_flag in userr.flag%TYPE,
    result out SYS_REFCURSOR
)
AS 
BEGIN
    OPEN RESULT FOR
      SELECT u.userid, u.nama, u.email, u.menuaccess, u.flag, u.username, m.merchantname
      FROM USERR u, merchant m
      WHERE m.merchantid = u.flag and u.flag = p_flag and u.isdeleted = '0'
      ORDER BY u.USERID ASC;
END GET_ALL_USERS;

/
--------------------------------------------------------
--  DDL for Procedure GET_ALL_USERS_BY_NAME
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."GET_ALL_USERS_BY_NAME" 
(
    p_name in userr.nama%TYPE,
    p_flag in userr.flag%TYPE,
    result out SYS_REFCURSOR
)
AS 
BEGIN
  OPEN RESULT FOR
    SELECT *
    FROM USERR
    WHERE NAMA LIKE '%' || p_name ||'%'
    AND flag = p_flag;
END GET_ALL_USERS_BY_NAME;

/
--------------------------------------------------------
--  DDL for Procedure GET_ALL_USER_EMAIL_USERNAME
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."GET_ALL_USER_EMAIL_USERNAME" 
(
    result out SYS_REFCURSOR
)
AS 
BEGIN
  OPEN RESULT FOR
    SELECT EMAIL, USERNAME
    FROM USERR;
END GET_ALL_USER_EMAIL_USERNAME;

/
--------------------------------------------------------
--  DDL for Procedure GET_LAST_FLAG
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."GET_LAST_FLAG" 
(
    result out SYS_REFCURSOR
)
AS 
BEGIN
  OPEN RESULT FOR
    SELECT merchantid 
    FROM merchant
    ORDER BY merchantid DESC;
END GET_LAST_FLAG;

/
--------------------------------------------------------
--  DDL for Procedure GET_USER_DETAIL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."GET_USER_DETAIL" 
(
    p_id in userr.userid%TYPE,
    p_flag in userr.flag%TYPE,
    result out SYS_REFCURSOR
)
AS 
BEGIN
  OPEN RESULT FOR
      SELECT u.userid, u.nama, u.email, u.menuaccess, u.flag, u.username, m.merchantname
      FROM USERR u, merchant m
      WHERE m.merchantid = u.flag and
      u.USERID = p_id and u.flag = p_flag;
END GET_USER_DETAIL;

/
--------------------------------------------------------
--  DDL for Procedure INSERT_PRODUCT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."INSERT_PRODUCT" 
(
    p_product_id    in product.productid%TYPE,
    p_nama          in product.nama%TYPE,
    p_qty           in product.qty%TYPE,
    p_harga         in product.harga%TYPE,
    p_kode          in product.kode%TYPE,
    p_merchant_id   in product.merchantid%TYPE,
    result out sys_refcursor
)
AS 
v_count_product_id number;
v_count_nama number;
v_count_kode number;
v_error_msg varchar2(100) := '';
v_error_msg_numb number;
BEGIN
    SELECT COUNT(*) into v_count_product_id FROM product WHERE LOWER(productid) = LOWER(p_product_id);
    SELECT COUNT(*) into v_count_nama FROM product WHERE LOWER(nama) = LOWER(p_nama) AND merchantid = p_merchant_id;
    SELECT COUNT(*) into v_count_kode FROM product WHERE LOWER(kode) = LOWER(p_kode) AND merchantid = p_merchant_id;

    IF (v_count_product_id = 0 AND v_count_nama = 0 AND v_count_kode = 0) THEN  
        INSERT INTO product(productid, nama, qty, harga, kode, merchantid)
        VALUES (p_product_id, p_nama, p_qty, p_harga, p_kode, p_merchant_id);
        v_error_msg := 'Produk berhasil dimasukkan';
        v_error_msg_numb := 1;
        
    ELSE IF (v_count_product_id != 0 AND v_count_nama = 0 AND v_count_kode = 0) THEN
        v_error_msg := 'ID produk sudah terdaftar';
        v_error_msg_numb := -1;
        
    ELSE IF (v_count_product_id = 0 AND v_count_nama != 0 AND v_count_kode = 0) THEN
        v_error_msg := 'Nama produk sudah terdaftar di merchant ini';
        v_error_msg_numb := 0;
        
    ELSE IF (v_count_product_id = 0 AND v_count_nama = 0 AND v_count_kode != 0) THEN
        v_error_msg := 'Kode produk sudah terdaftar di merchant ini';
        v_error_msg_numb := 0;
        
    ELSE
        v_error_msg := 'Nama, ID, atau kode produk sudah terdaftar di merchant ini';
        v_error_msg_numb := -2;
    END IF;
    END IF;
    END IF;
    END IF;
    
    OPEN RESULT FOR
        SELECT v_error_msg AS v_error_msg, v_error_msg_numb AS v_error_msg_numb
        FROM dual;
    COMMIT;
END INSERT_PRODUCT;

/
--------------------------------------------------------
--  DDL for Procedure INSERT_USER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."INSERT_USER" 
(
    p_nama in userr.nama%TYPE,
    p_password in userr.userpassword%TYPE,
    p_email in userr.email%TYPE,
    p_flag in userr.flag%TYPE,
    p_menuaccess in userr.menuaccess%TYPE,
    p_username in userr.username%TYPE,
    result out sys_refcursor
)
AS 
v_count_username number;
v_count_email number;
v_error_msg varchar2(100) := '';
v_error_msg_numb number;
BEGIN

    select count(username) into v_count_username from userr where username = p_username;
    select count(email) into v_count_email from userr where email = p_email;

    if ( v_count_username = 0 and v_count_email = 0) THEN  
        insert into userr (userid, nama, userpassword, email, flag, menuaccess, username, ISDELETED, isOwner)
        values (user_seq.nextval, p_nama, p_password, p_email, p_flag, p_menuaccess, p_username, '0', '0');
        v_error_msg_numb := 1;
            else if(v_count_username != 0 and v_count_email = 0) THEN
                v_error_msg := 'Username sudah terdaftar';
                v_error_msg_numb := -1;
                else if(v_count_username = 0 and v_count_email != 0) THEN
                    v_error_msg := 'Email sudah terdaftar';
                    v_error_msg_numb := 0;
                    else
                        v_error_msg := 'Email dan Username sudah terdaftar';
                        v_error_msg_numb := -2;
                end if;
            end if;
    end if;
    
    open result for
        select v_error_msg as v_error_msg,
                v_error_msg_numb as v_error_msg_numb
        from dual;
    
  commit;
END INSERT_USER;

/
--------------------------------------------------------
--  DDL for Procedure TEST_RICKY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."TEST_RICKY" 
(
    result out SYS_REFCURSOR
)
AS 
BEGIN
  OPEN RESULT FOR
    SELECT *
    FROM USERR;
END TEST_RICKY;

/
--------------------------------------------------------
--  DDL for Procedure UPDATE_PRODUCT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."UPDATE_PRODUCT" 
(
    p_product_id    IN product.productid%TYPE,
    p_nama          IN product.nama%TYPE,
    p_qty           IN product.qty%TYPE,
    p_harga         IN product.harga%TYPE,
    p_kode          IN product.kode%TYPE,
    p_merchant_id   IN product.merchantid%TYPE,
    result OUT sys_refcursor
)
AS 
v_count_product_id number;
v_count_nama number;
v_count_kode number;
v_error_msg varchar2(100) := '';
v_error_msg_numb number;
BEGIN
    SELECT COUNT(productid) into v_count_product_id FROM product WHERE LOWER(productid) = LOWER(p_product_id);
    SELECT COUNT(nama) into v_count_nama FROM product WHERE LOWER(nama) = LOWER(p_nama) AND merchantid = p_merchant_id AND LOWER(productid) != LOWER(p_product_id);
    SELECT COUNT(kode) into v_count_kode FROM product WHERE LOWER(kode) = LOWER(p_kode) AND merchantid = p_merchant_id AND LOWER(productid) != LOWER(p_product_id);

    IF (v_count_product_id != 0 AND v_count_nama = 0 AND v_count_kode = 0) THEN 
        UPDATE product
        SET nama = p_nama, qty = p_qty, harga = p_harga, kode = p_kode, merchantid = p_merchant_id
        WHERE LOWER(productid) = LOWER(p_product_id);
        v_error_msg := 'Produk berhasil diperbaharui';
        v_error_msg_numb := 1;
    
    ELSE IF (v_count_product_id = 0) THEN 
        v_error_msg := 'ID produk tidak ditemukan';
        v_error_msg_numb := -3;
        
    ELSE IF (v_count_product_id != 0 AND v_count_nama != 0 AND v_count_kode = 0) THEN
        v_error_msg := 'Nama produk sudah terdaftar di merchant ini';
        v_error_msg_numb := 0;
        
    ELSE IF (v_count_product_id != 0 AND v_count_nama = 0 AND v_count_kode != 0) THEN
        v_error_msg := 'Kode produk sudah terdaftar di merchant ini';
        v_error_msg_numb := 0;
        
    ELSE
        v_error_msg := 'Nama, ID, atau kode produk sudah terdaftar di merchant ini';
        v_error_msg_numb := -2;
    END IF;
    END IF;
    END IF;
    END IF;
    
    OPEN RESULT FOR
        SELECT v_count_product_id, v_count_nama, v_count_kode, v_error_msg AS v_error_msg, v_error_msg_numb AS v_error_msg_numb
        FROM dual;
    COMMIT;
END UPDATE_PRODUCT;

/
--------------------------------------------------------
--  DDL for Procedure UPDATE_USER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."UPDATE_USER" 
(
    p_id in userr.userid%TYPE,
    p_nama in userr.nama%TYPE,
    p_flag in USERR.flag%TYPE,
    p_menuaccess in USERR.MENUACCESS%TYPE,
    p_email in userr.email%TYPE,
    result out sys_refcursor
)
AS 
v_is_update number;
v_email_lama varchar2(100);
v_counter_email number;
v_error_msg varchar2(100) := '';
v_error_msg_numb number;
BEGIN
  select email into v_email_lama from userr where userid = p_id;
  
  if(v_email_lama = p_email) then
    UPDATE USERR SET NAMA = P_NAMA, menuaccess = p_menuaccess
    WHERE flag = p_flag and userid = p_id;
    v_is_update := SQL%ROWCOUNT;
              
              if(v_is_update = 0) THEN
                  v_error_msg := 'User tidak ditemukan';
                  v_error_msg_numb := 0;
                  else if(v_is_update = 1) THEN
                      v_error_msg := 'Update user berhasil';
                      v_error_msg_numb := 1;
                      else
                          v_error_msg := 'Kemungkinan ID Ganda, Silahkan dicek stored procedure UPDATE_USER dan DB User';
                          v_error_msg_numb := 0;
                  end if;
              end if;
    else
        select count(email) into v_counter_email from userr where email = p_email;
            if(v_counter_email = 0) THEN
              UPDATE USERR SET NAMA = P_NAMA, menuaccess = p_menuaccess, email = p_email
              WHERE flag = p_flag and userid = p_id;
              
              v_is_update := SQL%ROWCOUNT;
              
              if(v_is_update = 0) THEN
                  v_error_msg := 'User tidak ditemukan';
                  v_error_msg_numb := 0;
                  else if(v_is_update = 1) THEN
                      v_error_msg := 'Update user berhasil';
                      v_error_msg_numb := 1;
                      else
                          v_error_msg := 'Kemungkinan ID Ganda, Silahkan dicek stored procedure UPDATE_USER dan DB User';
                          v_error_msg_numb := 0;
                  end if;
              end if;
          else 
            v_error_msg := 'Email sudah terdaftar';
            v_error_msg_numb := 0;
          end if;
  end if;

  open result for
    select v_is_update as updated_row_count,
        v_error_msg as v_error_msg,
    v_error_msg_numb as v_error_msg_numb
    from dual;
  COMMIT;
END UPDATE_USER;

/
--------------------------------------------------------
--  DDL for Procedure UPDATE_USER_FOR_DELETE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "TEST"."UPDATE_USER_FOR_DELETE" 
(
    p_id in userr.userid%TYPE,
    p_flag in userr.flag%Type,
    result out sys_refcursor
)
AS 
v_is_update_delete number;
BEGIN
  UPDATE USERR SET ISDELETED = '1' where flag = p_flag and userid = p_id;
  
  v_is_update_delete := SQL%ROWCOUNT;
  
  open result for
    select v_is_update_delete as updated_row_count
    from dual;
  COMMIT;
END UPDATE_USER_FOR_DELETE;

/
--------------------------------------------------------
--  DDL for Package TEST_PRODUCT
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "TEST"."TEST_PRODUCT" AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  PROCEDURE INQUIRY_DETAIL_PRODUCT(
    p_id_product     IN  product.productid%TYPE,
    result           OUT SYS_REFCURSOR
  );
 
  PROCEDURE INQUIRY_ALL_PRODUCT(
    p_id_merchant    IN  product.merchantid%TYPE,
    result       OUT SYS_REFCURSOR
  );
 
  PROCEDURE ADD_PRODUCT(
    p_id_product    IN  product.productid%TYPE,
    p_nama          IN  product.nama%TYPE,
    p_qty           IN  product.qty%TYPE,
    p_harga         IN  product.harga%TYPE,
    p_kode          IN  product.kode%TYPE,
    p_id_merchant   IN  product.merchantid%TYPE,
    result          OUT SYS_REFCURSOR
  );
 
  PROCEDURE UPDATE_PRODUCT(
    p_id_product    IN  product.productid%TYPE,
    p_nama          IN  product.nama%TYPE,
    p_qty           IN  product.qty%TYPE,
    p_harga         IN  product.harga%TYPE,
    p_kode          IN  product.kode%TYPE,
    p_id_merchant   IN  product.merchantid%TYPE,
    result          OUT SYS_REFCURSOR
  );
 
  PROCEDURE DELETE_PRODUCT(
    p_id_product    IN  product.productid%TYPE,
    result          OUT SYS_REFCURSOR
  );

END TEST_PRODUCT;

/
--------------------------------------------------------
--  DDL for Package Body TEST_PRODUCT
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "TEST"."TEST_PRODUCT" AS

  PROCEDURE INQUIRY_DETAIL_PRODUCT(
    p_id_product     IN  product.productid%TYPE,
    result           OUT SYS_REFCURSOR
  ) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE BIT_BCHANT_PRODUCT.IQNUIRY_DETAIL_PRODUCT
    OPEN RESULT FOR
      SELECT productid, nama, qty, harga, kode, merchantid
      FROM product
      WHERE productid = p_id_product;
  END INQUIRY_DETAIL_PRODUCT;

  PROCEDURE INQUIRY_ALL_PRODUCT(
    p_id_merchant     IN  product.merchantid%TYPE,
    result       OUT SYS_REFCURSOR
  ) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE BIT_BCHANT_PRODUCT.INQUIRY_ALL_PRODUCT
    OPEN RESULT FOR
      SELECT productid, nama, qty, harga, kode, merchantid
      FROM product
      WHERE merchantid = p_id_merchant
      ORDER BY productid asc;
  END INQUIRY_ALL_PRODUCT;

  PROCEDURE ADD_PRODUCT(
    p_id_product    IN  product.productid%TYPE,
    p_nama          IN  product.nama%TYPE,
    p_qty           IN  product.qty%TYPE,
    p_harga         IN  product.harga%TYPE,
    p_kode          IN  product.kode%TYPE,
    p_id_merchant   IN  product.merchantid%TYPE,,
    result          OUT SYS_REFCURSOR)
    IS
    BEGIN
      INSERT INTO product(productid, nama, qty, harga, kode, merchantid)
      VALUES(p_id_product, p_nama, p_qty, p_harga, p_kode, p_id_merchant);
      
      IF (sql%rowcount) > 0 then
        OPEN RESULT FOR SELECT 'success' AS status FROM dual;
      ELSE
        OPEN RESULT FOR SELECT 'failed' AS status FROM dual;
      END IF;
    COMMIT;
  END ADD_PRODUCT;

  PROCEDURE UPDATE_PRODUCT(
    p_id_product    IN  product.productid%TYPE,
    p_nama          IN  product.nama%TYPE,
    p_qty           IN  product.qty%TYPE,
    p_harga         IN  product.harga%TYPE,
    p_kode          IN  product.kode%TYPE,
    p_id_merchant   IN  product.id_merchant%TYPE,
    result          OUT SYS_REFCURSOR)
    IS
    BEGIN
      UPDATE product
      SET nama=p_nama, qty=p_qty, harga=p_harga, kode=p_kode, merchantid=p_id_merchant
      WHERE productid=p_id_product;
      
      IF (sql%rowcount) > 0 then
        OPEN RESULT FOR SELECT 'success' AS status FROM dual;
      ELSE
        OPEN RESULT FOR SELECT 'failed' AS status FROM dual;
      END IF;
    COMMIT;
  END UPDATE_PRODUCT;

  PROCEDURE DELETE_PRODUCT(
    p_id_product    IN  product.productid%TYPE,
    result          OUT SYS_REFCURSOR)
    IS
    BEGIN
      DELETE FROM product
      WHERE productid=p_id_product;
      
      IF (sql%rowcount) > 0 then
        OPEN RESULT FOR SELECT 'success' AS status FROM dual;
      ELSE
        OPEN RESULT FOR SELECT 'failed' AS status FROM dual;
      END IF;
    COMMIT;
  END DELETE_PRODUCT;

END TEST_PRODUCT;

/
--------------------------------------------------------
--  Constraints for Table EMPLOYEE
--------------------------------------------------------

  ALTER TABLE "TEST"."EMPLOYEE" MODIFY ("EMPNAME" NOT NULL ENABLE);
  ALTER TABLE "TEST"."EMPLOYEE" MODIFY ("EMPID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MERCHANT
--------------------------------------------------------

  ALTER TABLE "TEST"."MERCHANT" ADD CONSTRAINT "MERCHANTS_PK" PRIMARY KEY ("MERCHANTID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "TEST"."MERCHANT" MODIFY ("MERCHANTID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CUSTOMER
--------------------------------------------------------

  ALTER TABLE "TEST"."CUSTOMER" ADD CONSTRAINT "CUSTOMER_PK" PRIMARY KEY ("CUSTOMERID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "TEST"."CUSTOMER" MODIFY ("CUSTOMERID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PRODUCT
--------------------------------------------------------

  ALTER TABLE "TEST"."PRODUCT" MODIFY ("MERCHANTID" NOT NULL ENABLE);
  ALTER TABLE "TEST"."PRODUCT" ADD CONSTRAINT "PRODUCT_PK" PRIMARY KEY ("PRODUCTID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "TEST"."PRODUCT" MODIFY ("PRODUCTID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MERCHANT_OLD
--------------------------------------------------------

  ALTER TABLE "TEST"."MERCHANT_OLD" MODIFY ("STATUS" NOT NULL ENABLE);
  ALTER TABLE "TEST"."MERCHANT_OLD" MODIFY ("ADDRESS" NOT NULL ENABLE);
  ALTER TABLE "TEST"."MERCHANT_OLD" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "TEST"."MERCHANT_OLD" MODIFY ("OWNERNAME" NOT NULL ENABLE);
  ALTER TABLE "TEST"."MERCHANT_OLD" MODIFY ("MERCHANTNAME" NOT NULL ENABLE);
  ALTER TABLE "TEST"."MERCHANT_OLD" ADD CONSTRAINT "MERCHANT_PK" PRIMARY KEY ("MERCHANTID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table SALES
--------------------------------------------------------

  ALTER TABLE "TEST"."SALES" ADD CONSTRAINT "SALES_PK" PRIMARY KEY ("SALESID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "TEST"."SALES" MODIFY ("SALESID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERR
--------------------------------------------------------

  ALTER TABLE "TEST"."USERR" MODIFY ("USERNAME" NOT NULL ENABLE);
  ALTER TABLE "TEST"."USERR" ADD CONSTRAINT "USERR_PK" PRIMARY KEY ("USERID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "TEST"."USERR" MODIFY ("MENUACCESS" NOT NULL ENABLE);
  ALTER TABLE "TEST"."USERR" MODIFY ("FLAG" NOT NULL ENABLE);
  ALTER TABLE "TEST"."USERR" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "TEST"."USERR" MODIFY ("USERPASSWORD" NOT NULL ENABLE);
  ALTER TABLE "TEST"."USERR" MODIFY ("NAMA" NOT NULL ENABLE);
  ALTER TABLE "TEST"."USERR" MODIFY ("USERID" NOT NULL ENABLE);
